import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Testes {
   //static final String FILE_TEST = "covid19.csv"; //Guarda em variavel a Localização ficheiro
    public static void main(String[] args ) throws ParseException, IOException, InterruptedException {
int []valicaotipoFicheiro=new int[2];

        String[] titulosAcumulado = new String[6];
        String[] titulosCasosDiarios = new String[6];
        // ######### Criar Array 1D de Datas String #####
        String[][] datasAcumulado = new String[950][2]; // Tamanho da estrutura de dados é 2,5 anos
        String[][] datasCasosDiarios = new String[950][2];   // Tamanho da estrutura de dados é 2,5 anos
        Main.preencherDatas("01-01-2020", "01-07-2020", datasAcumulado, datasCasosDiarios);
        // ######### Criar Array 2D de Dados Inteiros #####
        int[][] dadosAcumulado = new int[950][5];   // Tamanho da estrutura de dados é 2,5 anos
        int[][] dadosCasosDiarios = new int[950][5];    // Tamanho da estrutura de dados é 2,5 anos
        // PREENCHER ARRAYS DATAS
        Main.preencherDatas("01-01-2020", "01-07-2022", datasAcumulado, datasCasosDiarios);
        // LEITURA DE FICHEIRO

        String fileTest="Covid19.csv";
         Main.lerFicheiro(datasAcumulado,datasCasosDiarios,dadosAcumulado, dadosCasosDiarios,titulosCasosDiarios, titulosAcumulado,fileTest,valicaotipoFicheiro);


       // runTestes(testDatas,testTitulos, test_File, testDadosCovid);



    }
    // ############################  obterDescobrirTipoDeFicheiro  #########################################

    // ############################  obterLerFicheiro   #########################################

    // ############################  obterPreencherDatas   #########################################

    // ############################   obterDiaDaSemana  #########################################
    private static boolean test_obterDiaDaSemana(String data, int diaSemanaEsperado) throws ParseException {
        int diaSemana = Main.obterDiaDaSemana(data,1);
        //System.out.println(diaSemana);
        if(diaSemana==diaSemanaEsperado)
            return true;
        else
            return false;
    }

    // ############################   Obter Dia Do Mês #########################################
    private static boolean test_obterDiaDoMes(String data, int[] diaMesEsperado) throws ParseException {
        int[] diaMes = Main.obterDiaDoMes(data,1);
        if (Arrays.equals(diaMes, diaMesEsperado))
            return true;
        else
            return false;
    }




    // ############################   Mostrar Novos casos Diarios #########################################

    public static boolean test_mostrarNovosCasosDiarios(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint, int[][] novosCasosDiariosEsperado) throws IOException, InterruptedException {

        int[][]novosCasosDiariosEncontrado = Main.mostrarCasosTotaisDiario(datas, dadosCovid, dataInicio, dataFim, titulos, opcao, flagPrint);
        int contador=0;
        for (int i = 0; i < novosCasosDiariosEncontrado.length; i++) {
            for (int j = 0; j < novosCasosDiariosEncontrado.length; j++) {
                if (novosCasosDiariosEncontrado[i][j] == novosCasosDiariosEsperado[i][j]) {
                    contador = 0;
                } else {
                    contador++;
                }

            }
        }
        System.out.println("falhas:"+contador);
        if(contador>0){
            return false;}
        else{return true;}
    }

    // ############################   Mostrar Novos casos Semanais #########################################

    public static boolean test_mostrarNovosCasosSemanais(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint, int[][] novosCasosSemanaisEsperado) throws ParseException, IOException, InterruptedException {
       int[][] novosCasosSemanaisEncontrado = Main.mostrarCasosTotaisSemanal(datas, dadosCovid,dataInicio, dataFim, titulos, opcao, flagPrint);
       int contador = 0;
       for (int i = 0; i <novosCasosSemanaisEncontrado.length; i++) {
           for (int j = 0; j < novosCasosSemanaisEncontrado.length; j++) {
               if (novosCasosSemanaisEncontrado[i][j] == novosCasosSemanaisEsperado[i][j]) {
                   contador = 0;
               } else {
                   contador++;
               }
           }
       }
        System.out.println("falhas:"+contador);
       if (contador>0) {
           return false;
       } else {
           return true;
       }
    }

    // ############################   Mostrar Novos casos Mensais #########################################

    public static boolean test_mostrarNovosCasosMensais(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos,int opcao, boolean flagPrint, int[][] novosCasosMensaisEsperado) throws ParseException, IOException, InterruptedException {
        int[][] novosCasosMensaisEncontrado = Main.mostrarCasosTotaisMensal(datas, dadosCovid, dataInicio, dataFim, titulos, opcao, flagPrint);
        int contador = 0;
        for (int i = 0; i < novosCasosMensaisEncontrado.length; i++) {
            for (int j = 0; j < novosCasosMensaisEncontrado.length; i++) {
                if (novosCasosMensaisEncontrado[i][j] == novosCasosMensaisEsperado[i][j]) {
                    contador = 0;
                } else {
                    contador++;
                }
            }
        }
        System.out.println("falhas:"+contador);
        if (contador > 0) {
            return true;
        }else {
            return false;
        }
    }

    // ############################ mostrarAcumuladoDiarios    #########################################
    private static boolean test_mostrarAcumuladoDiarios(String[][] datasAcumulado, int[][] dadosAcumulado, String dataInicio, String dataFim, String[] titulosAcumulado, int opcao, boolean flagPrint, int[][] mostrarAcumuladoDiariosEsperado) throws IOException, InterruptedException {
      int[][] mostrarAcumuladoDiariosEncontrado = Main.mostrarAcumuladoDiarios(datasAcumulado, dadosAcumulado, dataInicio, dataFim, titulosAcumulado, opcao, flagPrint);
      int contador = 0;
      for (int i = 0; i < mostrarAcumuladoDiariosEncontrado.length; i++){
          for (int j = 0; j < mostrarAcumuladoDiariosEncontrado.length; j++){
              if (mostrarAcumuladoDiariosEncontrado[i][j] == mostrarAcumuladoDiariosEsperado[i][j]) {
                  contador = 0;
              } else {
                  contador++;
              }
          }
      }
        System.out.println(("falhas:"+contador));
      if (contador > 0) {
          return true;
      } else {
          return false;
      }
    }

    // ############################ mostrarAcumuladoSemanal   #########################################
    private static boolean test_mostrarAcumuladoSemanal(String[][] datasAcumulado, int[][] dadosAcumulado, String dataInicio, String dataFim, String[] titulosAcumulado, int opcao, boolean flagPrint, int[][] mostrarAcumuladoSemanalEsperado ) throws ParseException, IOException, InterruptedException {
        int[][] mostrarAcumuladoSemanalEncontrado = Main.mostrarAcumuladoSemanal(datasAcumulado, dadosAcumulado, dataInicio, dataFim, titulosAcumulado, opcao, flagPrint);
        int contador = 0;
        for (int i = 0; i < mostrarAcumuladoSemanalEncontrado.length; i++){
            for (int j = 0; j < mostrarAcumuladoSemanalEncontrado.length; j++){
                if (mostrarAcumuladoSemanalEncontrado[i][j] == mostrarAcumuladoSemanalEsperado[i][j]) {
                    contador = 0;
                } else {
                    contador++;
                }
            }
        }
        System.out.println("falhas:"+contador);
        if (contador > 0) {
            return true;
        }else {
            return false;
        }
    }

    // ############################ mostrarAcumuladoMensal    #########################################
    /*private static boolean test_mostrarAcumuladoMensal(String[][] datasAcumulado, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint) throws ParseException {
        int[][] mostratAcumuladoMensalEncontrado = Main.mostrarAcumuladoMensal(datasAcumulado, dadosCovid, dataInicio, dataFim, titulos, opcao, flagPrint);
        int contador=0;
        for (int i=0; i<mostrarAcumuladoMensalEncontrado.length; i++){
            for (int j=0; j< mostrarAcumuladoMensalEncontrado.length; j++){
                if (mostrarAcumuladoMensalEncontrado[i][j] ==)
            }
        }
    }*/



        // ############################   Metodo Procurar Data #########################################
    private static boolean test_descobrirData(String[][] datas, String data, int linhaDataEncontradaEsperada)
    {
        int linhaDataEncontrada = Main.descobrirData(datas, data);
        if(linhaDataEncontrada==linhaDataEncontradaEsperada)
            return true;
        else
            return false;
    }

    // ############################   Metodo Evolução Intervalo #########################################
    private static boolean test_diferencaEntreDatas(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, int[] diferencaEntreDatasEsperado) {
        int[] diferencaEntreDatas = Main.diferencaEntreDatas(datas, dadosCovid, dataInicio, dataFim);
        if(diferencaEntreDatas == diferencaEntreDatasEsperado)
            return true;
        else
            return false;
    }


    // ############################   Metodo Novo Array #########################################
    private static boolean test_novoArray(int[][] arrEntrada, int incremento, int[][] arrayEntradaEsperado) {
        int[][] arrayEntradaEncontrada = Main.gerarNovoArray(arrEntrada, incremento);
        if (arrayEntradaEncontrada == arrayEntradaEsperado)
            return true;
        else
            return false;
    }

    // ############################ calculoMediaTodosParametros    #########################################

    // ############################ calculoMedia    #########################################

    // ############################ calculoDesvioPadraoTodosParametros    #########################################

    // ############################ calcuiloDesvioPadrao    #########################################


    // ############################   Metodo Validar Datas #########################################
    private static boolean test_validacaoDatas(int linhaInicio, int linhaFim, String[][] datas, boolean validacaoDatasEsperado) {
        boolean dataValida = Main.validacaoDatas(linhaInicio, linhaFim, datas);
        if (dataValida == validacaoDatasEsperado)
            return true;
        else
            return false;
    }

    // ############################ metodoAnaliseComparativaNovosCasos #########################################

    // ############################ metodoAnaliseComparativaTodos #########################################

    // ############################   Metodo Analise Comparativa #########################################
    /*private static boolean test_analiseComparativa(boolean opcaoDiario, boolean opcaoSemanal, boolean opcaoMensal, String[][] datas, int[][] dadosCovid, String[] titulo,int[][] resultadoEsperado) throws ParseException {
        int[][] resultado = Main.metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datas, dadosCovid, titulo);
        if(resultado==resultadoEsperado)
            return true;
        else
            return false;
    }*/



    // ############################   Metodo Guardar 2DTXT #########################################
    private static boolean test_guardarEmFicheiroInt2DTXT(int[][] dados, String[]titulo, String nomeFicheiro, String guardarEmFicheiro2TXTEsperado) throws IOException {
        String guardarEmFicheiro2DTXTEncontrado = Main.guardarEmFicheiroInt2DTXT(dados, titulo, nomeFicheiro);
        if (guardarEmFicheiro2DTXTEncontrado == guardarEmFicheiro2TXTEsperado)
            return true;
        else
            return false;
    }


    // ############################   Metodo Guardar 1DTXT #########################################
    private static boolean test_guardarEmFicheiroInt1DTXT(int[] dados, String[] titulo, String nomeFicheiro, String guardarEmFicheiro1DTXTEsperado) throws IOException {
        String guardarEmFicheiro1DTXTEncontrado = Main.guardarEmFicheiroInt1DTXT(dados, titulo, nomeFicheiro);
        if (guardarEmFicheiro1DTXTEncontrado == guardarEmFicheiro1DTXTEsperado)
            return true;
        else
            return false;
    }

    // ############################ cadeiasDeMarkov #########################################

    // ############################ matrizTransposta #########################################

    // ############################ multiplicacaoMatizes #########################################

    // ############################ lerMatrizTransicao #########################################

    // ############################ checkData #########################################

    // ############################ metodoDeCrout #########################################

    // ############################ criarMatrizQ #########################################

    // ############################ criarMatrizIdentidade #########################################

    // ############################ criarMatrizNaoInversa #########################################



    public static void runTestes(String[][] testDatas, String[] testTitulos, String[][] testFile, int[][] testDadosCovid) throws ParseException, IOException, InterruptedException {
/*DONE*/        System.out.println( "Obter dia da Semana:" + (test_obterDiaDaSemana("2022-01-10",2)?"OK":"NOT OK"+"\n") );
/*DONE*/       System.out.println( "Obter dia do Mês:" + (test_obterDiaDoMes("2016-02-15", new int[]{15,29})?"OK":"NOT OK"+"\n") );
        //System.out.println("Obter Análise Diária:" + (test_analiseDiaria(new String[] {"2020-04-01", "2020-04-02", "2020-04-03", "2020-04-04"}, new int [][] {{10170223,8251,5664,1293,187},{10170850,9034,6706,1533,209},{10171394,9886,7764,1778,246}, {10172111,10524,8839,2029,266}}, "2020-04-01", "2020-04-04", new String[] {"diario_nao_infetado","acumulado_infetado","acumulado_hospitalizado","acumulado_internadoUCI","acumulado_mortes"}, new int[]{717,638,1075,251,20})?"OK":"NOT OK"+"\n"));
        //System.out.println("Obter Análise Semanal:" + (test_analiseSemanal()));
        //System.out.println("Obter Análise Mensal:" + (test_analiseMensal()));
/*DONE*/        System.out.println("Obter Novos Casos Diários:" + (test_mostrarNovosCasosDiarios(testDatas, testDadosCovid, "2020-04-05", "2020-04-06", testTitulos,1,false, new int[][]{{10173004,11278,9923,2296,295},{10173788,11730,11022,2566,311}})?"OK":"NOT OK"+"\n"));
                System.out.println("Obter Novos Casos Semanais:" + (test_mostrarNovosCasosSemanais(testDatas, testDadosCovid, "2020-04-05", "2020-04-06",testTitulos,1, false, new int[][]{{10173004,11278,9923,2296,295},{10173788,11730,11022,2566,311}})?"OK":"NOT OK"+"\n"));
        System.out.println("Obter Novos Casos Mensais:" + (test_mostrarNovosCasosMensais(testDatas, testDadosCovid, "2020-04-05", "2020-04-06", testTitulos, 1, false, new int[][]{{10173004,11278,9923,2296,295},{10173788,11730,11022,2566,311}})?"OK":"NOT OK"+"\n"));
/*DONE*/        System.out.println( "Obter data Encontrada:" + (test_descobrirData(new String[][]{{"2022-01-11", "2022-01-10", "2022-01-09"}},"2022-01-10",1) ?"OK":"NOT OK"+"\n") );
        //System.out.println("Obter Diferença Entre Datas:" + (test_diferencaEntreDatas()));
        //System.out.println( "Obter Ficheiro:" + (test_lerFicheiros(new String[][]{"Covid19.csv"},"2022-01-10",1) ?"OK":"NOT OK"+"\n") );
        //System.out.println("Validação Data:" + (test_validacaoDatas(1, 2, "2021-04-30",1)?"OK":"NOT OK"+"\n"));
        //System.out.println("Obter Análise Comparativa:" + (test_analiseComparativa()));
        //System.out.println("Obter Guardar Ficheiro em CSV:" + (test_guardarEmFicheiro1DCSV()));
        //System.out.println("Obter Guardar Ficheiro em 2D TXT:" + (test_guardarEmFicheiroInt2DTXT()));
        //System.out.println("Obter Guardar Ficheiro em 1D TXT:" + (test_guardarEmFicheiroInt1DTXT()));



    }
}
