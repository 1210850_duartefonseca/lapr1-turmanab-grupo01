import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {
    // ############################# VARIAVEIS GLOBAIS ################################################################
    static Scanner ler = new Scanner(System.in);

    //static final String FILE_NAME = "Covid19.csv"; //Guarda em variavel a Localização ficheiro
    public static void metodoEnter() {
        System.out.println("Carregue \"ENTER\" para continuar...");
        Scanner ler = new Scanner(System.in);
        ler.nextLine();
    }

    public static void appLogo() {

        System.out.println("\n\n");

        System.out.println("  |______LOGO!_______|");
        System.out.println("  from @the Mighty Squad 2022");
        System.out.println("\n\n");

    }

    public static void main(String[] args) throws IOException, FileNotFoundException, ParseException, InterruptedException {
        int[] valicaotipoFicheiro = new int[2];
        // Dia inicial do projeto 01-01-2020
        String[] titulosAcumulado = new String[6];
        String[] titulosCasosDiarios = new String[6];
        // ######### Criar Array 1D de Datas String #####
        String[][] datasAcumulado = new String[950][2]; // Tamanho da estrutura de dados é 2,5 anos
        String[][] datasCasosDiarios = new String[950][2];   // Tamanho da estrutura de dados é 2,5 anos
        // ######### Criar Array 2D de Dados Inteiros #####
        int[][] dadosAcumulado = new int[950][5];   // Tamanho da estrutura de dados é 2,5 anos
        int[][] dadosCasosDiarios = new int[950][5];    // Tamanho da estrutura de dados é 2,5 anos
        // PREENCHER ARRAYS DATAS
        preencherDatas("01-01-2020", "08-08-2022", datasAcumulado, datasCasosDiarios);
        // ######### INTERAÇÃO ############
        String fileName = "";

        if (args.length == 0) {
            appLogo();
            System.out.println("\n\n\t\t\tMetodo interativo\n\n");
            System.out.println("\n\n\t\t\tDigite o nome do ficheiro a ler: \n\n");
            fileName = ler.nextLine();
            if (fileName.toLowerCase().equals("sair")) {
                System.exit(-1);
            }
            File ficheiro = new File(fileName);
            verificarFicheiro(ficheiro);
            lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
            menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);

            menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
        } else {
            System.out.println("\n\n\t\t\tMétodo nao Interativo\n\n");
            modoNaoInterativoLerArgumentos(args, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
        }
        // ############################   MENUS  #########################################
        menu(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
    }

    //------------------------------------- MÉTODO PARA DESCOBRIR TIPO DE FICHEIRO --------------------------------------------------
    public static int descobrirTipoDeFicheiro(String file) throws FileNotFoundException {
        int tipoDeFicheiro = -1;
        Scanner ler = new Scanner(new File(file));
        String[] linhaUm = ler.nextLine().trim().split(",");
        String[] colunaTres = linhaUm[2].trim().split("_");
        String palavraChave = colunaTres[0];
        ler.close();
        if (palavraChave.equalsIgnoreCase("acumulado")) {
            tipoDeFicheiro = 0;
        }
        if (palavraChave.equalsIgnoreCase("infetados")) {
            tipoDeFicheiro = 1;
        }
        return tipoDeFicheiro; // Retorna um inteiro
    }

    //------------------------------------- MÉTODO ler Ficheiros --------------------------------------------------
    public static void lerFicheiro(String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, String file_name, int[] valicaotipoFicheiro) throws IOException, ParseException, InterruptedException {
        int cont = 0;
        // descobrir o tipo de ficheiro
        int tipoDeFicheiro = descobrirTipoDeFicheiro(file_name);
        // ler data inicial do ficheiro para verificar se já foi inserida
        Scanner ler = new Scanner(new File(file_name));
        ler.nextLine();
        String[] linhaDois = ler.nextLine().trim().split(",");
        String dataInicialDoFicheiro = linhaDois[0];
        ler.close();
        // leitura de dados para as matrizes e arrays corretos;
        Scanner ler2 = new Scanner(new File(file_name));
        if (tipoDeFicheiro == 0) {
            System.out.println("Ficheiro de Acumulados carregado em memoria..");// Ficheiro de Acumulados
            int linhaDaDataInicialDoFicheiro = descobrirDataParaEntradaDeFicheiros(datasAcumulado, dataInicialDoFicheiro);
            titulosAcumulado = ler2.nextLine().trim().split(",");
            while (ler2.hasNextLine()) {
                String[] line = ler2.nextLine().trim().split(",");
                datasAcumulado[linhaDaDataInicialDoFicheiro][0] = line[0];
                datasAcumulado[linhaDaDataInicialDoFicheiro][1] = "1";
                for (int coluna = 1; coluna < line.length; coluna++) {
                    dadosAcumulado[linhaDaDataInicialDoFicheiro][coluna - 1] = Integer.parseInt(line[coluna]);
                }
                linhaDaDataInicialDoFicheiro++;
            }
            valicaotipoFicheiro[0] = 1;
        }
        if (tipoDeFicheiro == 1) {
            System.out.println("Ficheiro de Casos totais carregado em memoria..");// Ficheiro de Novos Casos Diarios
            int linhaDaDataInicialDoFicheiro = descobrirDataParaEntradaDeFicheiros(datasCasosDiarios, dataInicialDoFicheiro);
            titulosCasosDiarios = ler2.nextLine().trim().split(",");
            while (ler2.hasNextLine()) {
                String[] line = ler2.nextLine().trim().split(",");
                datasCasosDiarios[linhaDaDataInicialDoFicheiro][0] = line[0];
                datasCasosDiarios[linhaDaDataInicialDoFicheiro][1] = "1";
                for (int coluna = 1; coluna < line.length; coluna++) {
                    dadosCasosDiarios[linhaDaDataInicialDoFicheiro][coluna - 1] = Integer.parseInt(line[coluna]);
                }
                linhaDaDataInicialDoFicheiro++;
            }
            valicaotipoFicheiro[1] = 1;
        }
        ler2.close(); // Fecha o ficheiro
    }
    //------------------------------------- MÉTODO preencher arrays datas --------------------------------------------------
    public static void preencherDatas(String startDate, String endDate, String[][] datasAcumulado, String[][] datasCasosDiarios) throws ParseException {
        Date dataInicio = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
        Date dataFimCasosDiarios = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
        int cont = 0;
        DateFormat formatarStringData = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat formatarStringDataCasosDiarios = new SimpleDateFormat("dd-MM-yyyy");
        String dataResultado = "";
        String dataResultadoCasosDiarios = "";
        Calendar calendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(dataFimCasosDiarios);
        calendar.setTime(dataInicio);
        while (calendar.before(endCalendar)) {
            dataInicio = calendar.getTime();
            dataResultado = formatarStringData.format(dataInicio);
            dataResultadoCasosDiarios = formatarStringDataCasosDiarios.format(dataInicio);
            datasAcumulado[cont][0] = dataResultado;
            datasAcumulado[cont][1] = "0";
            datasCasosDiarios[cont][1] = "0";
            datasCasosDiarios[cont][0] = dataResultadoCasosDiarios;
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            cont++;
        }
    }

    // ############################   Verificar se Ficheiro existe  #########################################
    public static void verificarFicheiro(File ficheiro) throws IOException, InterruptedException {
        if (ficheiro.exists()) {
            System.out.println("O ficheiro " + ficheiro + " existe.");
        } else {
            System.out.println("O ficheiro " + ficheiro + " nao existe.");
            System.out.println("Programa irá encerrar...");
            metodoEnter();
            limparEcra();
            return;
        }
    }

    //#################################### MODO NAO INTERATIVO ########################################################


    public static void modoNaoInterativoLerArgumentos(String[] args, String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int[] valicaotipoFicheiro) throws IOException, InterruptedException, ParseException {
        String TotaisCSV = "", AcumuladosCSV = "", matrizTransiçãoTXT = "", ficheiroSaidaTXT = "", dataInicio = "", dataFim = "", dataInicialPeriodoUm = "",
                dataFinalPeriodoUm = "", diaPrevisão = "", dataInicialPeriodoDois = "", dataFinalPeriodoDois = "", diaTotalDeCasos = "";
        int resolucaoTemporal = 0;
        int naoInterativoOpcao = 0;
        //############################################### MODO1 #############################################################
        if (args[(args.length - 4)].endsWith(".csv") && args[(args.length - 3)].endsWith(".csv") && args[(args.length - 2)].endsWith(".txt") && args[(args.length - 1)].endsWith(".txt")) {
            naoInterativoOpcao = 1;
            metodoEnter();
            try {
                for (int argumentos = 0; argumentos < args.length; argumentos++) {
                    if ((args[argumentos]).equals("-r")) {
                        resolucaoTemporal = Integer.parseInt(args[argumentos + 1]); // 0 DIARIO 1 SEMANAL 2 MENSAL
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di")) {  //DATA INICIO PARA VISUALIZAR APENAS
                        dataInicio = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df")) { //DATA FIM PARA VISUALIZAR APENAS
                        dataFim = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di1")) {
                        dataInicialPeriodoUm = args[argumentos + 1]; // DATA INICIO 1º INTERVALO PARA ANALISE COMPARATIVA
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df1")) {  // DATA FINAL 1º INTERVALO PARA ANALISE COMPARATIVA
                        dataFinalPeriodoUm = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di2")) {  // DATA INICIO 2º INTERVALO PARA ANALISE COMPARATIVA
                        dataInicialPeriodoDois = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df2")) {    // DATA INICIO 2º INTERVALO PARA ANALISE COMPARATIVA
                        dataFinalPeriodoDois = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-T")) {  // DATA PARA VER PREVISÃO
                        diaPrevisão = args[argumentos + 1];
                        argumentos++;

                    } else if (argumentos == (args.length - 4)) { //FICHEIRO TOTAIS
                        TotaisCSV = args[argumentos];

                    } else if (argumentos == (args.length - 3)) { //FICHEIRO ACUMULADOS
                        AcumuladosCSV = args[argumentos];

                    } else if (argumentos == (args.length - 2)) { // MATRIZ PARA PREVISÃO
                        matrizTransiçãoTXT = args[argumentos];

                    } else if (argumentos == (args.length - 1)) { // FICHEIRO PARA GUARDAR
                        ficheiroSaidaTXT = args[argumentos];
                    }
                }
            } catch (Exception e) {
                System.out.println("Argumentos inválidos.");
                System.exit(-1);
            }
            //########################################## MODO 2 ##################################################################
        } else if (args[(args.length - 3)].endsWith(".csv") && args[(args.length - 2)].endsWith(".txt") && args[(args.length - 1)].endsWith(".txt")) {

            naoInterativoOpcao = 2;
            metodoEnter();
            try {
                for (int argumentos = 0; argumentos < args.length; argumentos++) {
                    if ((args[argumentos]).equals("-T")) {  // DATA PARA VER PREVISÃO
                        diaPrevisão = args[argumentos + 1];
                        argumentos++;

                    } else if (argumentos == (args.length - 3)) { //FICHEIRO TOTAIS
                        TotaisCSV = args[argumentos];

                    } else if (argumentos == (args.length - 2)) { // MATRIZ PARA PREVISÃO
                        matrizTransiçãoTXT = args[argumentos];

                    } else if (argumentos == (args.length - 1)) { // FICHEIRO PARA GUARDAR
                        ficheiroSaidaTXT = args[argumentos];
                    }
                }
                if (resolucaoTemporal < 0 || resolucaoTemporal > 2) {
                    System.out.println("Resolução temporal errado. ");
                }

            } catch (Exception e) {
                System.out.println("Argumentos inválidos.");
                System.exit(-1);
            }

            //######################################## MODO 3 ####################################################################
        } else if (args[(args.length - 2)].endsWith(".csv") && args[(args.length - 1)].endsWith(".txt")) {

            naoInterativoOpcao = 3;
            metodoEnter();
            try {
                for (int argumentos = 0; argumentos < args.length; argumentos++) {
                    if ((args[argumentos]).equals("-r")) {
                        resolucaoTemporal = Integer.parseInt(args[argumentos + 1]); // 0 DIARIO 1 SEMANAL 2 MENSAL
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di")) {  //DATA INICIO PARA VISUALIZAR APENAS
                        dataInicio = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df")) { //DATA FIM PARA VISUALIZAR APENAS
                        dataFim = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di1")) {
                        dataInicialPeriodoUm = args[argumentos + 1]; // DATA INICIO 1º INTERVALO PARA ANALISE COMPARATIVA
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df1")) {  // DATA FINAL 1º INTERVALO PARA ANALISE COMPARATIVA
                        dataFinalPeriodoUm = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-di2")) {  // DATA INICIO 2º INTERVALO PARA ANALISE COMPARATIVA
                        dataInicialPeriodoDois = args[argumentos + 1];
                        argumentos++;

                    } else if ((args[argumentos]).equals("-df2")) {    // DATA INICIO 2º INTERVALO PARA ANALISE COMPARATIVA
                        dataFinalPeriodoDois = args[argumentos + 1];
                        argumentos++;

                    } else if (argumentos == (args.length - 2)) { //FICHEIRO ACUMULADOS
                        AcumuladosCSV = args[argumentos];

                    } else if (argumentos == (args.length - 1)) { // FICHEIRO PARA GUARDAR
                        ficheiroSaidaTXT = args[argumentos];
                    }
                }
                if (resolucaoTemporal < 0 || resolucaoTemporal > 2) {
                    System.out.println("Resolução temporal errado. ");
                }

            } catch (Exception e) {
                System.out.println("Argumentos inválidos.");
                System.exit(-1);
            }

        } else {
            System.out.println("Formato dos ficheiros inválido.");
            metodoEnter();
            System.exit(-1);
        }
        //titulos
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número Total de Infetados";
        titulosOutput[2] = "Número Total de Hospitalizados";
        titulosOutput[3] = "Número Total de Internados em UCI";
        titulosOutput[4] = "Número Total de Óbitos";
        String[] titulosOutput2 = new String[6];
        titulosOutput2[0] = "Data";
        titulosOutput2[1] = "Número de Novos Casos";
        titulosOutput2[2] = "Número de Novas Hospitalizações";
        titulosOutput2[3] = "Número de Novos Internados em UCI";
        titulosOutput2[4] = "Número de Óbitos";

        //Ficheiro de Saida
        String filename = ficheiroSaidaTXT;
        FileWriter myWriter = new FileWriter(filename, true);
        PrintWriter escritor = new PrintWriter(myWriter);


        int[][] resultadosmostrarAcumuladoDiarios;
        int[][] resultadosmostrarCasosTotaisDiario;
        //semanal
        int[][] resultadosmostrarAcumuladoSemanal;
        int[][] resultadosmostrarCasosTotaisSemanal;
        //mensal
        int[][] resultadosmostrarAcumuladoMensal;
        int[][] resultadosmostrarCasosTotaisMensal;
        //comparativa
        int[][] resultadosmostrarAcumuladoComparativa;
        int[][] resultadosmostrarCasosTotaisComparativa;

        boolean naoInterativo = true;

        //##############carregar o ficheiro TODOS os parametros##################
        System.out.println(TotaisCSV);
        System.out.println(AcumuladosCSV);
        if (naoInterativoOpcao == 1) {
            System.out.println("O programa ira mostrar/processar e efectuar previsao para dados de Covid19 ");
            metodoEnter();
            //Opcao com mostrar,comparativa,previsao para ambos os ficheiros

            //Carregar Ficheiro AcumuladosCSV
            File ficheiro = new File(AcumuladosCSV);
            verificarFicheiro(ficheiro);
            lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, AcumuladosCSV, valicaotipoFicheiro);
            //Carregar Ficheiro TotaisCSV
            File ficheiro2 = new File(TotaisCSV);
            verificarFicheiro(ficheiro2);
            lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, TotaisCSV, valicaotipoFicheiro);
            //Metodos mostrar dados
            //diarios


            if (resolucaoTemporal == 0) {
                Date di = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicio);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date df = new SimpleDateFormat("dd-MM-yyyy").parse(dataFim);
                String dataInicioF = dateFormat.format(di);
                String dataFimF = dateFormat.format(df);
                int linhaData1 = descobrirData(datasAcumulado, dataInicioF);
                int linhaData2 = descobrirData(datasAcumulado, dataFimF);
                boolean dataValida = validacaoDatas(linhaData1, linhaData2, datasAcumulado);
                if (dataValida == false) {
                    System.out.println("Data Invalida para mostrar novos casos");
                    metodoEnter();
                    metodoSair();
                }
                if (AcumuladosCSV != "") {
                    resultadosmostrarAcumuladoDiarios = mostrarAcumuladoDiarios(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);

                    //######################GUARDAR EM FICHEIRO###################################
                    escritor.printf("####### Resultados Mostrar Acumulado Diarios ############\n");
                    escritor.println();
                    for (int j = 0; j < titulosOutput2.length-1; j++) {
                        escritor.printf(" %33s |", titulosOutput2[j]);
                    }
                    escritor.println();
                    for (int i = 0; i < resultadosmostrarAcumuladoDiarios.length; i++) {
                        resultadosmostrarAcumuladoDiarios[i][0] = linhaData1;
                        escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                        for (int j = 1; j < resultadosmostrarAcumuladoDiarios[0].length; j++) {
                            escritor.printf(" | %33d", resultadosmostrarAcumuladoDiarios[i][j]);
                        }
                        escritor.println(" |");
                        linhaData1++;
                    }
                    //############################################################################

                }

                resultadosmostrarCasosTotaisDiario = mostrarCasosTotaisDiario(datasCasosDiarios, dadosCasosDiarios, dataInicio, dataFim, titulosCasosDiarios, 5, true);

                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Totais Diarios ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarCasosTotaisDiario.length; i++) {
                    resultadosmostrarCasosTotaisDiario[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasCasosDiarios[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarCasosTotaisDiario[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarCasosTotaisDiario[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################

                escritor.println();
            } else if (resolucaoTemporal == 1) {
                Date di = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicio);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date df = new SimpleDateFormat("dd-MM-yyyy").parse(dataFim);
                String dataInicioF = dateFormat.format(di);
                String dataFimF = dateFormat.format(df);
                int linhaData1 = descobrirData(datasAcumulado, dataInicioF);
                int linhaData2 = descobrirData(datasAcumulado, dataFimF);
                boolean dataValida = validacaoDatas(linhaData1, linhaData2, datasAcumulado);
                if (dataValida == false) {
                    System.out.println("Data Invalida para mostrar novos casos");
                    metodoEnter();
                    metodoSair();
                }

                resultadosmostrarAcumuladoSemanal = mostrarAcumuladoSemanal(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);

                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Acumulado Semanal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoSemanal.length; i++) {
                    resultadosmostrarAcumuladoSemanal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoSemanal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoSemanal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################

                limparEcra();
                resultadosmostrarCasosTotaisSemanal = mostrarCasosTotaisSemanal(datasCasosDiarios, dadosCasosDiarios, dataInicio, dataFim, titulosCasosDiarios, 5, true);

                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Totais Semanal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarCasosTotaisSemanal.length; i++) {
                    resultadosmostrarCasosTotaisSemanal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasCasosDiarios[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarCasosTotaisSemanal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarCasosTotaisSemanal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################

                limparEcra();
            } else if (resolucaoTemporal == 2) {
                Date di = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicio);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date df = new SimpleDateFormat("dd-MM-yyyy").parse(dataFim);
                String dataInicioF = dateFormat.format(di);
                String dataFimF = dateFormat.format(df);
                int linhaData1 = descobrirData(datasAcumulado, dataInicioF);
                int linhaData2 = descobrirData(datasAcumulado, dataFimF);
                boolean dataValida = validacaoDatas(linhaData1, linhaData2, datasAcumulado);
                if (dataValida == false) {
                    System.out.println("Data Invalida para mostrar novos casos");
                    metodoEnter();
                    metodoSair();
                }
                resultadosmostrarAcumuladoMensal = mostrarAcumuladoMensal(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);


                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Acumulado Mensal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoMensal.length; i++) {
                    resultadosmostrarAcumuladoMensal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", dadosAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoMensal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoMensal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################


                limparEcra();
                resultadosmostrarCasosTotaisMensal = mostrarCasosTotaisMensal(datasCasosDiarios, dadosCasosDiarios, dataInicio, dataFim, titulosCasosDiarios, 5, true);

                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar totais Mensal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarCasosTotaisMensal.length; i++) {
                    resultadosmostrarCasosTotaisMensal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasCasosDiarios[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarCasosTotaisMensal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarCasosTotaisMensal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################


                limparEcra();
            } else {
                System.out.println("Resolucao temporal invalida");
                metodoEnter();
                metodoSair();
            }
            //Metodos analise comparativa
            if (dataInicialPeriodoUm != "" && dataFinalPeriodoUm != "" && dataInicialPeriodoDois != "" && dataFinalPeriodoDois != "") {

                if (AcumuladosCSV != "") {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date di1 = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicialPeriodoUm);
                    Date df1 = new SimpleDateFormat("dd-MM-yyyy").parse(dataFinalPeriodoUm);
                    Date di2 = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicialPeriodoDois);
                    Date df2 = new SimpleDateFormat("dd-MM-yyyy").parse(dataFinalPeriodoDois);
                    String dataInicialPeriodoUmF = dateFormat.format(di1);
                    String dataFinalPeriodoUmF = dateFormat.format(df1);
                    String dataInicialPeriodoDoisF = dateFormat.format(di2);
                    String dataFinalPeriodoDoisF = dateFormat.format(df2);
                    int linhaData1 = descobrirData(datasAcumulado, dataInicialPeriodoUmF);
                    int linhaData2 = descobrirData(datasAcumulado, dataFinalPeriodoUmF);
                    int linhaData3 = descobrirData(datasAcumulado, dataInicialPeriodoDoisF);
                    int linhaData4 = descobrirData(datasAcumulado, dataFinalPeriodoDoisF);
                    boolean dataValida1 = validacaoDatas(linhaData1, linhaData2, datasAcumulado);
                    boolean dataValida2 = validacaoDatas(linhaData3, linhaData4, datasAcumulado);
                    if (dataValida2 == false || dataValida1 == false) {
                        System.out.println("Data Invalida para mostrar novos casos");
                        metodoEnter();
                        metodoSair();
                    }
                    resultadosmostrarAcumuladoComparativa = metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, 5, 1, naoInterativo, dataInicialPeriodoUmF, dataFinalPeriodoUmF, dataInicialPeriodoDoisF, dataFinalPeriodoDoisF);
                    limparEcra();

                    //######################GUARDAR EM FICHEIRO###################################
                    escritor.printf("####### Resultados Comparativa Acumulados ############\n");
                    escritor.println();
                    for (int j = 0; j < titulosOutput2.length-1; j++) {
                        escritor.printf(" %33s |", titulosOutput2[j]);
                    }
                    escritor.println();
                    for (int i = 0; i < resultadosmostrarAcumuladoComparativa.length; i++) {
                        // resultadosmostrarAcumuladoComparativa[i][0] = linhaData1;
                        // escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                        for (int j = 1; j < resultadosmostrarAcumuladoComparativa[0].length; j++) {
                            escritor.printf(" | %33d", resultadosmostrarAcumuladoComparativa[i][j]);
                        }
                        escritor.println(" |");
                        linhaData1++;
                    }
                    //################################      ############################################

                }

                resultadosmostrarCasosTotaisComparativa = metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, 5, 2, naoInterativo, dataInicialPeriodoUm, dataFinalPeriodoUm, dataInicialPeriodoDois, dataFinalPeriodoDois);


                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Comparativa Totais ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarCasosTotaisComparativa.length; i++) {
                    //resultadosmostrarCasosTotaisComparativa[i][0] = linhaData1;
                    // escritor.printf("                   Dia: %10s", datasCasosDiarios[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarCasosTotaisComparativa[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarCasosTotaisComparativa[i][j]);
                    }
                    escritor.println(" |");
                    //linhaData1++;
                }
                //############################################################################


                limparEcra();

                String matrizDeInput = matrizTransiçãoTXT;
                double[][] matrizTransicao = lerMatrizTransicao(matrizDeInput);  // criar matriz de transição

                cadeiasDeMarkov(dadosCasosDiarios, datasCasosDiarios, matrizTransicao);
                metodoDeCrout(dadosCasosDiarios, matrizTransicao);



            } else {
                System.out.println("Parametros intervalo de datas em falta..");
                metodoEnter();
                metodoSair();
            }
            //############################################################################################################
            //############################ apenas com ficheiro AcumuladosCSV##############################################
        } else if (naoInterativoOpcao == 3) {
            //carregar apenas ficheiro acumulados
            System.out.println("O programa ira mostrar/processar dados de covid19 utilizando dados acumulados ");
            metodoEnter();
            File ficheiro = new File(AcumuladosCSV);
            verificarFicheiro(ficheiro);
            lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, AcumuladosCSV, valicaotipoFicheiro);
            //formatar data para metodo acumulados
            Date di = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicio);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date df = new SimpleDateFormat("dd-MM-yyyy").parse(dataFim);
            String dataInicioF = dateFormat.format(di);
            String dataFimF = dateFormat.format(df);
            int linhaData1 = descobrirData(datasAcumulado, dataInicioF);
            int linhaData2 = descobrirData(datasAcumulado, dataFimF);
            boolean dataValida1 = validacaoDatas(linhaData1, linhaData2, datasAcumulado);

            if (dataValida1 == false) {
                System.out.println("Data Invalida para mostrar novos casos");
                metodoEnter();
                metodoSair();
            }


            if (resolucaoTemporal == 0) {
                resultadosmostrarAcumuladoDiarios = mostrarAcumuladoDiarios(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);
                //######################GUARDAR EM FICHEIRO###################################

                escritor.printf("####### Resultados Mostrar Acumulado Diarios ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoDiarios.length; i++) {
                    resultadosmostrarAcumuladoDiarios[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoDiarios[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoDiarios[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }

                //############################################################################
                limparEcra();
            } else if (resolucaoTemporal == 1) {
                resultadosmostrarAcumuladoSemanal = mostrarAcumuladoSemanal(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);
                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Acumulado Semanal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoSemanal.length; i++) {
                    resultadosmostrarAcumuladoSemanal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoSemanal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoSemanal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //############################################################################
                limparEcra();
            } else if (resolucaoTemporal == 2) {
                resultadosmostrarAcumuladoMensal = mostrarAcumuladoMensal(datasAcumulado, dadosAcumulado, dataInicioF, dataFimF, titulosAcumulado, 5, true);

                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Mostrar Acumulado Mensal ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoMensal.length; i++) {
                    resultadosmostrarAcumuladoMensal[i][0] = linhaData1;
                    escritor.printf("                   Dia: %10s", dadosAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoMensal[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoMensal[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }

                limparEcra();

            } else {
                System.out.println("Resolucao temporal invalida..");
                metodoEnter();
                metodoSair();
            }
            //Metodos analise comparativa
            if (dataInicialPeriodoUm != "" && dataFinalPeriodoUm != "" && dataInicialPeriodoDois != "" && dataFinalPeriodoDois != "") {
                Date di1 = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicialPeriodoUm);
                Date df1 = new SimpleDateFormat("dd-MM-yyyy").parse(dataFinalPeriodoUm);
                Date di2 = new SimpleDateFormat("dd-MM-yyyy").parse(dataInicialPeriodoDois);
                Date df2 = new SimpleDateFormat("dd-MM-yyyy").parse(dataFinalPeriodoDois);
                String dataInicialPeriodoUmF = dateFormat.format(di1);
                String dataFinalPeriodoUmF = dateFormat.format(df1);
                String dataInicialPeriodoDoisF = dateFormat.format(di2);
                String dataFinalPeriodoDoisF = dateFormat.format(df2);
                int linhaData1C = descobrirData(datasAcumulado, dataInicialPeriodoUmF);
                int linhaData2C = descobrirData(datasAcumulado, dataFinalPeriodoUmF);
                int linhaData3C = descobrirData(datasAcumulado, dataInicialPeriodoDoisF);
                int linhaData4C = descobrirData(datasAcumulado, dataFinalPeriodoDoisF);
                boolean dataValida1C = validacaoDatas(linhaData1C, linhaData2C, datasAcumulado);
                boolean dataValida2C = validacaoDatas(linhaData3C, linhaData4C, datasAcumulado);
                if (dataValida1C == false || dataValida2C == false) {
                    System.out.println("Data Invalida para mostrar novos casos");
                    metodoEnter();
                    metodoSair();
                }
                resultadosmostrarAcumuladoComparativa = metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, 5, 1, naoInterativo, dataInicialPeriodoUmF, dataFinalPeriodoUmF, dataInicialPeriodoDoisF, dataFinalPeriodoDoisF);
                //######################GUARDAR EM FICHEIRO###################################
                escritor.printf("####### Resultados Comparativa Acumulados ############\n");
                escritor.println();
                for (int j = 0; j < titulosOutput2.length-1; j++) {
                    escritor.printf(" %33s |", titulosOutput2[j]);
                }
                escritor.println();
                for (int i = 0; i < resultadosmostrarAcumuladoComparativa.length; i++) {
                    // resultadosmostrarAcumuladoComparativa[i][0] = linhaData1;
                    // escritor.printf("                   Dia: %10s", datasAcumulado[linhaData1][0]);
                    for (int j = 1; j < resultadosmostrarAcumuladoComparativa[0].length; j++) {
                        escritor.printf(" | %33d", resultadosmostrarAcumuladoComparativa[i][j]);
                    }
                    escritor.println(" |");
                    linhaData1++;
                }
                //################################      ############################################
                limparEcra();

            } else {
                System.out.println("Parametros intervalo de datas em falta..");
                metodoEnter();
                metodoSair();
            }

        }
        //############################ apenas com ficheiro TotaisCSV##############################################
        else if(naoInterativoOpcao==2) {
            System.out.println("O programa ira efectuar previsao de dadoscovid19 utilizando dados totais ");
            //carregar apenas ficheiro TotaisCSV
            metodoEnter();
            File ficheiro2 = new File(TotaisCSV);
            verificarFicheiro(ficheiro2);
            lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, TotaisCSV, valicaotipoFicheiro);
            String matrizDeInput = matrizTransiçãoTXT;
            double[][] matrizTransicao = lerMatrizTransicao(matrizDeInput);  // criar matriz de transição

            cadeiasDeMarkov(dadosCasosDiarios, datasCasosDiarios, matrizTransicao);
            metodoDeCrout(dadosCasosDiarios, matrizTransicao);


            //############################################################################################################
        }else {
            if(naoInterativoOpcao==0){
                System.out.println("ERRO Formato do comando invalido...");
            }
            metodoEnter();
            metodoSair();
        }

        escritor.close();
        //Fim modo nao interativo
        System.out.println("Dados processados, programa ira sair...");

        metodoEnter();
        metodoSair();

    }

    // ############################   obterDiaDaSemana  #########################################
    public static int obterDiaDaSemana(String data, int tipoFicheiro) throws ParseException {
        Date date;
        if (tipoFicheiro == 1) {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(data);
        } else {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(data);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    // ############################   Obter Dia Do Mês #########################################
    public static int[] obterDiaDoMes(String data, int tipoFicheiro) throws ParseException {
        //tipoFicheiro 1 acumulados
        //tipoFicheiro 2 totais
        int[] diaAtualEUltimoDiaDoMes = new int[2];
        Date currentDayOfMonth;
        if (tipoFicheiro == 1) {
            currentDayOfMonth = new SimpleDateFormat("yyyy-MM-dd").parse(data);
        } else {
            currentDayOfMonth = new SimpleDateFormat("dd-MM-yyyy").parse(data);
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDayOfMonth);
        diaAtualEUltimoDiaDoMes[0] = cal.get(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DATE, -1);
        diaAtualEUltimoDiaDoMes[1] = cal.get(Calendar.DAY_OF_MONTH);
        return diaAtualEUltimoDiaDoMes;
    }

    // ############################   Mostrar Novos casos Diarios #########################################

    public static int[][] mostrarCasosTotaisDiario(String[][] datasCasosDiarios, int[][] dadosCasosDiarios, String dataInicio, String dataFim, String[] tituloCasosDiarios, int opcao, boolean flagPrint) throws IOException, InterruptedException {
        int linhaInicio = descobrirData(datasCasosDiarios, dataInicio);
        int linhaFim = descobrirData(datasCasosDiarios, dataFim);
        int numTabelas = dadosCasosDiarios[0].length;
        int[][] dadosDiarios = new int[linhaFim - linhaInicio + 1][numTabelas];
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número Total de Infetados";
        titulosOutput[2] = "Número Total de Hospitalizados";
        titulosOutput[3] = "Número Total de Internados em UCI";
        titulosOutput[4] = "Número Total de Óbitos";
        //Preencher -1

        int totalLinhas = (linhaFim - linhaInicio + 1);
        for (int i = 0; i < totalLinhas; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosDiarios[i][j] = -1;
            }
        }
        if (!flagPrint) {
            System.out.printf("Resultados Dados Diários entre as Datas: %s - %s:\n", datasCasosDiarios[linhaInicio][0], datasCasosDiarios[linhaFim][0]);
        }

        switch (opcao) {
            // 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasCasosDiarios[linhaInicio][0]);
                    }
                    for (int j = 1; j < numTabelas; j++) {
                        dadosDiarios[i][j] = dadosCasosDiarios[linhaInicio][j];
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosCasosDiarios[linhaInicio][j]);
                        }
                    }
                    if (!flagPrint) {
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasCasosDiarios[linhaInicio][0]);
                    }
                    dadosDiarios[i][4] = dadosCasosDiarios[linhaInicio][4];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCasosDiarios[linhaInicio][4]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasCasosDiarios[linhaInicio][0]);
                    }
                    dadosDiarios[i][3] = dadosCasosDiarios[linhaInicio][3];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCasosDiarios[linhaInicio][3]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasCasosDiarios[linhaInicio][0]);
                    }
                    dadosDiarios[i][2] = dadosCasosDiarios[linhaInicio][2];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCasosDiarios[linhaInicio][2]);
                        System.out.println();
                    }
                    linhaInicio++;
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasCasosDiarios[linhaInicio][0]);
                    }
                    dadosDiarios[i][1] = dadosCasosDiarios[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCasosDiarios[linhaInicio][1]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");

        }
        if (!flagPrint) {
            System.out.println("------------------------------------------");
        }
        if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosDiarios, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }


        return dadosDiarios;
    }


    // ############################   Mostrar Novos casos Semanal #########################################
    public static int[][] mostrarCasosTotaisSemanal(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint) throws ParseException, IOException, InterruptedException {
        /*
        Discobrir linhas das datas no array de Datas
         */
        int linhaInicio = descobrirData(datas, dataInicio);
        if (datas[linhaInicio - 1][1].equals("0")) {
            linhaInicio++;
            System.out.printf("Não existe dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n", datas[linhaInicio - 1][0], datas[linhaInicio][0]);
        }
        int linhaFim = descobrirData(datas, dataFim);
        int numTabelas = 5;
        int sizeL = 950;
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número Total de Infetados";
        titulosOutput[2] = "Número Total de Hospitalizados";
        titulosOutput[3] = "Número Total de Internados em UCI";
        titulosOutput[4] = "Número Total de Óbitos";

        /*Numero de dias em cada semana*/
        int totalLinhas = 7;
        int cont = -1;
        int[] arrayDiaDaSemana = new int[sizeL - 1];
        int[] arrayPosicaoSemanasCompletas = new int[sizeL / 7];
        /*
        Cilcos para encontrar o 1 dia de cada semana completa
         */

        for (int linhas = linhaInicio; linhas < linhaFim; linhas++) {
            arrayDiaDaSemana[linhas] = obterDiaDaSemana(datas[linhas][0], 2);
        }
        for (int linhas = linhaInicio; linhas < linhaFim; linhas++) {
            if (arrayDiaDaSemana[linhas] == 2 && arrayDiaDaSemana[linhas + 6] == 1) {
                cont++;
                arrayPosicaoSemanasCompletas[cont] = linhas;
            }
        }
        int totalSemanas = cont + 1; //total de semanas completas
        int[][] dadosSemana = new int[totalSemanas][numTabelas];
        /*
        Prencher matrix com valor -1 para distinguir posiçoes preenchidas
        */
        for (int i = 0; i < totalSemanas; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosSemana[i][j] = -1;
            }
        }
        // Opcoes disponiveis no menu | 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
        if (!flagPrint) {
            System.out.printf("Resultados Dados Semanais entre as Datas: %s - %s:\n", datas[linhaInicio][0], datas[linhaFim][0]);
        }

        /*
        Switch para cada opcao previamente seleciona fazer operacao de soma para cada dia da semana
        */
        switch (opcao) {
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalSemanas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf("Semana %2d: %10s a %10s", semanas + 1, datas[linhaInicio][0], datas[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    for (int j = 1; j < numTabelas - 1; j++) {
                        dadosSemana[semanas][j] = 0;
                        linhaFim = linhaInicio + 7;
                        for (int i = linhaInicio; i < linhaFim; i++) {
                            dadosSemana[semanas][j] = dadosSemana[semanas][j] + (dadosCovid[i][j] - dadosCovid[i - 1][j]);
                        }
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosSemana[semanas][j]);
                        }
                    }
                    dadosSemana[semanas][4] = 0;
                    linhaFim = linhaInicio + 7;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosSemana[semanas][4] = dadosSemana[semanas][4] + dadosCovid[i][4];
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosSemana[semanas][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf("Semana %2d: %10s a %10s", semanas + 1, datas[linhaInicio][0], datas[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][4] = 0;
                    linhaFim = linhaInicio + 7;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosSemana[semanas][4] = dadosSemana[semanas][4] + dadosCovid[i][4];
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCovid[linhaInicio][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf("Semana %2d: %10s a %10s", semanas + 1, datas[linhaInicio][0], datas[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][3] = 0;
                    linhaFim = linhaInicio + 7;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosSemana[semanas][3] = dadosSemana[semanas][3] + (dadosCovid[i][3] - dadosCovid[i - 1][3]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCovid[linhaInicio][3]);
                        System.out.println(" |");
                    }
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[2]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf("Semana %2d: %10s a %10s", semanas + 1, datas[linhaInicio][0], datas[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][2] = 0;
                    linhaFim = linhaInicio + 7;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosSemana[semanas][2] = dadosSemana[semanas][2] + (dadosCovid[i][2] - dadosCovid[i - 1][2]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %32d", dadosCovid[linhaInicio][2]);
                        System.out.println(" |");
                    }
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[1]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf("Semana %2d: %10s a %10s", semanas + 1, datas[linhaInicio][0], datas[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][2] = 0;
                    linhaFim = linhaInicio + 7;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosSemana[semanas][1] = dadosSemana[semanas][1] + (dadosCovid[i][1] - dadosCovid[i - 1][1]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosCovid[linhaInicio][1]);
                        System.out.println(" |");
                    }
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");
        }
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosSemana, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }
        return dadosSemana;
    }

    //------------------------------------- Mostrar Novos casos Mensal --------------------------------------------------
    public static int[][] mostrarCasosTotaisMensal(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint) throws ParseException, IOException, InterruptedException {
        int linhaInicio = descobrirData(datas, dataInicio);
        if (datas[linhaInicio - 1][1].equals("0")) {
            linhaInicio++;
            System.out.printf("Não é possivel obter os dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n", datas[linhaInicio - 1][0], datas[linhaInicio][0]);
        }
        int linhaFim = descobrirData(datas, dataFim);
        int numTabelas = 5;
        int sizeL = 950;
        int cont = -1;
        int contadorLinhas = 0;
        // Array com os Titulos de Output
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número Total de Infetados";
        titulosOutput[2] = "Número Total de Hospitalizados";
        titulosOutput[3] = "Número Total de Internados em UCI";
        titulosOutput[4] = "Número Total de Óbitos";

        int diaFinalDoMesAtual = 0;
        int[][] arrayDiaDoMes = new int[sizeL][2];
        int[] arrayPosicaoMesCompletos = new int[sizeL / 29];
        for (int linhas = linhaInicio; linhas <= linhaFim; linhas++) {
            arrayDiaDoMes[linhas] = obterDiaDoMes(datas[linhas][0], 2);
        }
        for (int linhas = linhaInicio; linhas <= linhaFim; linhas++) {
            diaFinalDoMesAtual = arrayDiaDoMes[linhas][1];
            if (linhas + (diaFinalDoMesAtual - 1) <= linhaFim) {
                if (arrayDiaDoMes[linhas][0] == 1 && arrayDiaDoMes[linhas + (diaFinalDoMesAtual - 1)][0] == diaFinalDoMesAtual) {
                    cont++;
                    arrayPosicaoMesCompletos[cont] = linhas;
                }
            }
        }
        int totalMeses = cont + 1;

        int[][] dadosMensal = new int[totalMeses][numTabelas];
        // Prencher matrix com valor -1 para distinguir posiçoes preenchidas
        for (int i = 0; i < totalMeses; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosMensal[i][j] = -1;
            }
        }
        // 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
        // Print titulos
        if (!flagPrint) {
            System.out.printf("Resultados Dados Mensais entre as Datas: %s - %s:\n", datas[linhaInicio][0], datas[linhaFim][0]);
        }
        switch (opcao) {
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datas[linhaInicio][0], datas[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    for (int j = 1; j < numTabelas - 1; j++) {
                        linhaFim = linhaInicio + totalDeDiasDoMes;
                        dadosMensal[meses][j] = 0;
                        for (int i = linhaInicio; i < linhaFim; i++) {
                            dadosMensal[meses][j] = dadosMensal[meses][j] + (dadosCovid[i][j] - dadosCovid[i - 1][j]);
                        }
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosMensal[meses][j]);
                        }
                    }
                    dadosMensal[meses][4] = 0;
                    linhaFim = linhaInicio + totalDeDiasDoMes;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosMensal[meses][4] = dadosMensal[meses][4] + dadosCovid[i][4];
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datas[linhaInicio][0], datas[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    linhaFim = linhaInicio + totalDeDiasDoMes;
                    dadosMensal[meses][4] = 0;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosMensal[meses][4] = dadosMensal[meses][4] + (dadosCovid[i][4] - dadosCovid[i - 1][4]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datas[linhaInicio][0], datas[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    linhaFim = linhaInicio + totalDeDiasDoMes;
                    dadosMensal[meses][3] = 0;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosMensal[meses][3] = dadosMensal[meses][3] + (dadosCovid[i][3] - dadosCovid[i - 1][3]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][3]);
                        System.out.println(" |");
                    }
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[2]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datas[linhaInicio][0], datas[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    linhaFim = linhaInicio + totalDeDiasDoMes;
                    dadosMensal[meses][2] = 0;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosMensal[meses][2] = dadosMensal[meses][2] + (dadosCovid[i][2] - dadosCovid[i - 1][2]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][2]);
                        System.out.println(" |");
                    }
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[1]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datas[linhaInicio][0], datas[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    linhaFim = linhaInicio + totalDeDiasDoMes;
                    dadosMensal[meses][1] = 0;
                    for (int i = linhaInicio; i < linhaFim; i++) {
                        dadosMensal[meses][1] = dadosMensal[meses][1] + (dadosCovid[i][1] - dadosCovid[i - 1][1]);
                    }
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][1]);
                        System.out.println(" |");
                    }
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");
        }
        System.out.println("------------------------------------------");
        if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosMensal, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }
        return dadosMensal;
    }

    // Mostrar Novos casos em cada dia
    public static int[][] mostrarAcumuladoDiarios(String[][] datasAcumulado, int[][] dadosAcumulado, String dataInicio, String dataFim, String[] titulosAcumulado, int opcao, boolean flagPrint) throws IOException, InterruptedException {
        int linhaInicio = descobrirData(datasAcumulado, dataInicio);
        if (datasAcumulado[linhaInicio - 1][1].equals("0")) {
            linhaInicio++;
            System.out.printf("Não é possivel obter dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n", datasAcumulado[linhaInicio - 1][0], datasAcumulado[linhaInicio][0]);
        }
        int linhaFim = descobrirData(datasAcumulado, dataFim);
        int numTabelas = dadosAcumulado[0].length;
        System.out.println(linhaInicio);
        System.out.println(linhaFim);
        int[][] dadosDiarios = new int[linhaFim - linhaInicio + 1][numTabelas];
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número de Novos Casos";
        titulosOutput[2] = "Número de Novas Hospitalizações";
        titulosOutput[3] = "Número de Novos Internados em UCI";
        titulosOutput[4] = "Número de Óbitos";
        //Preencher -1

        int totalLinhas = (linhaFim - linhaInicio + 1);
        for (int i = 0; i < totalLinhas; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosDiarios[i][j] = -1;
            }
        }
        if (!flagPrint) {
            System.out.printf("Resultados Novos Dados Diários entre as Datas: %s - %s:\n", datasAcumulado[linhaInicio][0], datasAcumulado[linhaFim][0]);
        }

        switch (opcao) {
            // 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasAcumulado[linhaInicio][0]);
                    }
                    for (int j = 1; j < numTabelas; j++) {
                        dadosDiarios[i][j] = dadosAcumulado[linhaInicio][j] - dadosAcumulado[linhaInicio - 1][j];
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosDiarios[i][j]);
                        }
                    }
                    if (!flagPrint) {
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasAcumulado[linhaInicio][0]);
                    }
                    dadosDiarios[i][4] = dadosAcumulado[linhaInicio][4] - dadosAcumulado[linhaInicio - 1][4];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosDiarios[i][4]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasAcumulado[linhaInicio][0]);
                    }
                    dadosDiarios[i][3] = dadosAcumulado[linhaInicio][3] - dadosAcumulado[linhaInicio - 1][3];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosDiarios[i][3]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasAcumulado[linhaInicio][0]);
                    }
                    dadosDiarios[i][2] = dadosAcumulado[linhaInicio][2] - dadosAcumulado[linhaInicio - 1][2];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosDiarios[i][2]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int i = 0; i < totalLinhas; i++) {
                    dadosDiarios[i][0] = linhaInicio;
                    if (!flagPrint) {
                        System.out.printf("                   Dia: %10s", datasAcumulado[linhaInicio][0]);
                    }
                    dadosDiarios[i][1] = dadosAcumulado[linhaInicio][1] - dadosAcumulado[linhaInicio - 1][1];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosDiarios[i][1]);
                        System.out.println(" |");
                    }
                    linhaInicio++;
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");

        }
        if (!flagPrint) {
            System.out.println("------------------------------------------");
        }
       if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosDiarios, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }


        return dadosDiarios;
    }


    // ############################   Mostrar Novos casos Semanal #########################################
    public static int[][] mostrarAcumuladoSemanal(String[][] datasAcumulado, int[][] dadosAcumulado, String dataInicio, String dataFim, String[] titulosAcumulado, int opcao, boolean flagPrint) throws ParseException, IOException, InterruptedException {
              /*
        Descobrir linhas das datas no array de Datas
         */
        int linhaInicio = descobrirData(datasAcumulado, dataInicio);
        if (datasAcumulado[linhaInicio - 1][1].equals("-1")) {
            linhaInicio++;
            System.out.printf("Não é possivel obter dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n", datasAcumulado[linhaInicio - 1][0], datasAcumulado[linhaInicio][0]);
        }
        int linhaFim = descobrirData(datasAcumulado, dataFim);
        int numTabelas = 5;
        int sizeL = 950;
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número de Novos Casos";
        titulosOutput[2] = "Número de Novas Hospitalizações";
        titulosOutput[3] = "Número de Novas Internados em UCI";
        titulosOutput[4] = "Número de Óbitos";

        /*Numero de dias em cada semana*/
        int totalLinhas = 7;
        int cont = -1;
        int[] arrayDiaDaSemana = new int[sizeL - 1];
        int[] arrayPosicaoSemanasCompletas = new int[sizeL / 7];
        /*
        Cilcos para encontrar o 1 dia de cada semana completa
         */

        for (int linhas = linhaInicio; linhas < linhaFim; linhas++) {
            arrayDiaDaSemana[linhas] = obterDiaDaSemana(datasAcumulado[linhas][0], 1);
        }
        for (int linhas = linhaInicio; linhas < linhaFim; linhas++) {
            if (arrayDiaDaSemana[linhas] == 2 && arrayDiaDaSemana[linhas + 6] == 1) {
                cont++;
                arrayPosicaoSemanasCompletas[cont] = linhas;
            }
        }

        int totalSemanas = cont + 1; //total de semanas completas
        int[][] dadosSemana = new int[totalSemanas][numTabelas];
        /*
        Prencher matrix com valor -1 para distinguir posiçoes preenchidas
        */
        for (int i = 0; i < totalSemanas; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosSemana[i][j] = -1;
            }
        }
        // Opcoes disponiveis no menu | 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
        if (!flagPrint) {
            System.out.printf("Resultados de Novos Dados Semanais entre as Datas: %s - %s:\n", datasAcumulado[linhaInicio][0], datasAcumulado[linhaFim][0]);
        }

        /*
        Switch para cada opcao previamente seleciona fazer operacao de soma para cada dia da semana
        */
        switch (opcao) {
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalSemanas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf(" Semana %d: %10s a %10s", semanas + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    for (int j = 1; j < numTabelas; j++) {
                        dadosSemana[semanas][j] = dadosAcumulado[linhaInicio + 6][j] - dadosAcumulado[linhaInicio - 1][j];
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosSemana[semanas][j]);
                        }
                    }
                    if (!flagPrint) {
                        System.out.println(" |");
                    }
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf(" Semana %d: %10s a %10s", semanas + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][4] = 0;
                    int linhaFinal = linhaInicio + 6;
                    dadosSemana[semanas][4] = dadosAcumulado[linhaInicio + 6][4] - dadosAcumulado[linhaInicio - 1][4];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosSemana[semanas][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf(" Semana %d: %10s a %10s", semanas + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][3] = dadosAcumulado[linhaInicio + 6][3] - dadosAcumulado[linhaInicio - 1][3];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosAcumulado[linhaInicio][3]);
                        System.out.println(" |");
                    }
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[2]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf(" Semana %d: %10s a %10s", semanas + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][2] = dadosAcumulado[linhaInicio + 6][2] - dadosAcumulado[linhaInicio - 1][2];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosAcumulado[linhaInicio][2]);
                        System.out.println(" |");
                    }
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[1]);
                    System.out.println();
                }
                for (int semanas = 0; semanas < totalLinhas; semanas++) {
                    linhaInicio = arrayPosicaoSemanasCompletas[semanas];
                    if (!flagPrint) {
                        System.out.printf(" Semana %d: %10s a %10s", semanas + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + 6][0]);
                    }
                    dadosSemana[semanas][0] = linhaInicio;
                    dadosSemana[semanas][1] = dadosAcumulado[linhaInicio + 6][1] - dadosAcumulado[linhaInicio - 1][1];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosAcumulado[linhaInicio][1]);
                        System.out.println(" |");
                    }
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");

        }
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosSemana, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }
        return dadosSemana;
    }

    //------------------------------------- Mostrar Acumulado Mensal --------------------------------------------------
    public static int[][] mostrarAcumuladoMensal(String[][] datasAcumulado, int[][] dadosCovid, String dataInicio, String dataFim, String[] titulos, int opcao, boolean flagPrint) throws ParseException, IOException, InterruptedException {
        int linhaInicio = descobrirData(datasAcumulado, dataInicio);
        if (datasAcumulado[linhaInicio - 1][1].equals("0")) {
            linhaInicio++;
            System.out.printf("Não é possivel obter dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n", datasAcumulado[linhaInicio - 1][0], datasAcumulado[linhaInicio][0]);
        }
        int linhaFim = descobrirData(datasAcumulado, dataFim);
        int numTabelas = 5;
        int sizeL = 950;
        int cont = -1;
        int contadorLinhas = 0;
        // Array com os Titulos de Output
        String[] titulosOutput = new String[6];
        titulosOutput[0] = "Data";
        titulosOutput[1] = "Número de Novos Casos";
        titulosOutput[2] = "Número de Novas Hospitalizações";
        titulosOutput[3] = "Número de Novos Internados em UCI";
        titulosOutput[4] = "Número de Óbitos";

        int diaFinalDoMesAtual = 0;
        int[][] arrayDiaDoMes = new int[sizeL][2];
        int[] arrayPosicaoMesCompletos = new int[sizeL / 29];
        for (int linhas = linhaInicio; linhas <= linhaFim; linhas++) {
            arrayDiaDoMes[linhas] = obterDiaDoMes(datasAcumulado[linhas][0], 1);
        }
        for (int linhas = linhaInicio; linhas <= linhaFim; linhas++) {
            diaFinalDoMesAtual = arrayDiaDoMes[linhas][1];
            if (linhas + (diaFinalDoMesAtual - 1) <= linhaFim) {
                if (arrayDiaDoMes[linhas][0] == 1 && arrayDiaDoMes[linhas + (diaFinalDoMesAtual - 1)][0] == diaFinalDoMesAtual) {
                    cont++;
                    arrayPosicaoMesCompletos[cont] = linhas;
                }
            }
        }
        int totalMeses = cont + 1;
        int[][] dadosMensal = new int[totalMeses][numTabelas];
        // Prencher matrix com valor -1 para distinguir posiçoes preenchidas
        for (int i = 0; i < totalMeses; i++) {
            for (int j = 0; j < numTabelas; j++) {
                dadosMensal[i][j] = -1;
            }
        }
        // 1-Novos Casos | 2-Novas Hospitalizações | 3-Novos Internados UCI | 4-Óbitos Diários | 5-Todos os anteriores
        if (!flagPrint) {
            System.out.printf("Resultados de Novos Dados Mensais entre as Datas: %s - %s:\n", datasAcumulado[linhaInicio][0], datasAcumulado[linhaFim][0]);
        }
        switch (opcao) {
            case 5:
                if (!flagPrint) {
                    for (int j = 0; j < numTabelas; j++) {
                        System.out.printf(" %33s |", titulosOutput[j]);
                    }
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("    Mês %d: %10s a %10s", meses + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    for (int j = 1; j < numTabelas; j++) {
                        dadosMensal[meses][j] = dadosCovid[linhaInicio + totalDeDiasDoMes - 1][j] - dadosCovid[linhaInicio - 1][j];
                        if (!flagPrint) {
                            System.out.printf(" | %33d", dadosMensal[meses][j]);
                        }
                    }
                    if (!flagPrint) {
                        System.out.println(" |");
                    }
                }
                break;
            case 4:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[4]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("Mês %d: %10s a %10s ", meses + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    dadosMensal[meses][4] = dadosCovid[linhaInicio + totalDeDiasDoMes - 1][4] - dadosCovid[linhaInicio - 1][4];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][4]);
                        System.out.println(" |");
                    }
                }
                break;
            case 3:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[3]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("Mês %d: %10s a %10s ", meses + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    dadosMensal[meses][4] = dadosCovid[linhaInicio + totalDeDiasDoMes - 1][3] - dadosCovid[linhaInicio - 1][3];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][3]);
                        System.out.println(" |");
                    }
                }
                break;
            case 2:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[2]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("Mês %d: %10s a %10s ", meses + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    dadosMensal[meses][2] = dadosCovid[linhaInicio + totalDeDiasDoMes - 1][2] - dadosCovid[linhaInicio - 1][2];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][2]);
                        System.out.println(" |");
                    }
                }
                break;
            case 1:
                if (!flagPrint) {
                    System.out.printf(" %33s |", titulosOutput[0]);
                    System.out.printf(" %33s |", titulosOutput[1]);
                    System.out.println();
                }
                for (int meses = 0; meses < totalMeses; meses++) {
                    linhaInicio = arrayPosicaoMesCompletos[meses];
                    int totalDeDiasDoMes = arrayDiaDoMes[linhaInicio][1];
                    if (!flagPrint) {
                        System.out.printf("Mês %d: %10s a %10s ", meses + 1, datasAcumulado[linhaInicio][0], datasAcumulado[linhaInicio + totalDeDiasDoMes - 1][0]);
                    }
                    dadosMensal[meses][0] = linhaInicio;
                    dadosMensal[meses][1] = dadosCovid[linhaInicio + totalDeDiasDoMes - 1][1] - dadosCovid[linhaInicio - 1][1];
                    if (!flagPrint) {
                        System.out.printf(" | %33d", dadosMensal[meses][1]);
                        System.out.println(" |");
                    }
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");
        }
        System.out.println("---------------------------------------------------------------------------------");
        if(!flagPrint) {
            float[] mediaMatrix = new float[5];
            float[] desvioPadrao = new float[5];
            questionarFicheiro(dadosMensal, titulosOutput, 1, mediaMatrix, desvioPadrao, false);
        }
        return dadosMensal;
    }

    //------------------------------------- MÉTODO PROCURAR DATA --------------------------------------------------
    public static int descobrirDataParaEntradaDeFicheiros(String[][] datas, String data) {
        int linhasDataEncontrada = -1;
        int linhas = datas.length;
        for (int linha = 0; linha < linhas; linha++) {
            if (datas[linha][0].equalsIgnoreCase(data)) {
                linhasDataEncontrada = linha;
            }
        }
        return linhasDataEncontrada;
    }

    //------------------------------------- MÉTODO PROCURAR DATA --------------------------------------------------
    public static int descobrirData(String[][] datas, String data) {
        int linhasDataEncontrada = -1;
        int linhas = datas.length;
        for (int linha = 0; linha < linhas; linha++) {
            if (datas[linha][0].equalsIgnoreCase(data) && datas[linha][1] == "1") {
                linhasDataEncontrada = linha;
            }
        }
        return linhasDataEncontrada;
    }

    //------------------------------------- MÉTODO EVOLUÇÂO INTERVALO--------------------------------------------------
    public static int[] diferencaEntreDatas(String[][] datas, int[][] dadosCovid, String dataInicio, String dataFim) { //Metodo para gerar novo Array
        int posicaoDataInicio = descobrirData(datas, dataInicio);
        int posicaoDataFim = descobrirData(datas, dataFim);
        int aux;
        int colunas = dadosCovid[0].length;
        int[] resultado = new int[colunas];

        if (posicaoDataInicio > posicaoDataFim) {
            System.out.println("Erro. Data de inicio superior à final");
        }
        for (int coluna = 0; coluna < colunas; coluna++) {
            resultado[coluna] = dadosCovid[posicaoDataFim][coluna] - dadosCovid[posicaoDataInicio][coluna];
        }

        return resultado;
    }


    //------------------------------------- MÉTODO PARA O NOVO ARRAY --------------------------------------------------
    public static int[][] gerarNovoArray(int[][] arrEntrada, int incremento) { //Metodo para gerar novo Array
        int sizeL = arrEntrada.length;
        int sizeC = arrEntrada[0].length;
        int[][] arraySaida = new int[sizeL][sizeC]; //Criar novo array com dimensões iguais ao anterior
        for (int linha = 0; linha < sizeL; linha++) {
            for (int coluna = 0; coluna < sizeC; coluna++) {

                arraySaida[linha][coluna] = arrEntrada[linha][coluna] + incremento; //Incrementa um valor ao array
            }
        }

        return arraySaida;
    }

    // ---------------------------------- IMPRESSÃO DO ARRAY STRING---------------------------------------------------------
    public static void printFuncString(String[][] file) {

        for (int linha = 0; linha < file.length; linha++) {
            for (int coluna = 0; coluna < file[linha].length; coluna++)
                System.out.printf("%30s", file[linha][coluna]);

            System.out.println();
        }
    }

    public static void printFuncString1D(String[] file) {

        for (int linha = 0; linha < file.length; linha++) {
            System.out.printf("%30s", file[linha]);
        }
        System.out.println();
    }

    public static void printFuncString1DTitulos(String[] file) {

        for (int linha = 1; linha < file.length; linha++) {
            System.out.printf("%30s", file[linha]);
        }
        System.out.println();
    }

    public static void printFuncInt1D(int[] file) {
        for (int linha = 0; linha < file.length; linha++) {
            System.out.printf("%10d", file[linha]);
        }
    }

    public static void printFunc2DINTNot0(int[][] file) {
//Doesnt Print -1!
        for (int linha = 0; linha < file.length; linha++) {
            for (int coluna = 0; coluna < file[linha].length; coluna++)
                if (file[linha][coluna] != -1) {
                    System.out.printf("%30d", file[linha][coluna]);
                }
            System.out.println();
        }
    }

    public static void printFunc2DINTLinha(int[][] file, int linhaInicio) {
//Doesnt Print -1!
        //faz print a parti de certa linha , linhaInicio

        for (int linha = linhaInicio; linha < file.length; linha++) {
            for (int coluna = 0; coluna < file[linha].length; coluna++)
                if (file[linha][coluna] != -1) {
                    System.out.printf("%30d", file[linha][coluna]);
                }
            System.out.println();
        }
    }

    public static void printFunc2DINT(int[][] file) {

        for (int linha = 0; linha < file.length; linha++) {
            for (int coluna = 0; coluna < file[linha].length; coluna++)
                System.out.printf("%30d", file[linha][coluna]);

            System.out.println();
        }
    }
    // ----------------------------------  Menu ---------------------------------------------------------

    public static void menu(String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] tituloCasosDiarios, String[] titulosAcumulado, int[] valicaotipoFicheiro) throws ParseException, IOException, InterruptedException {
        metodoEnter();
        limparEcra();
        menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, tituloCasosDiarios, titulosAcumulado, valicaotipoFicheiro);

    }

    // ----------------------------------  Menu Principal---------------------------------------------------------
    public static void menuPrincipal(String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int[] valicaotipoFicheiro) throws ParseException, IOException, InterruptedException {
        int escolha;
        metodoEnter();
        limparEcra();
        System.out.printf("######################################################################################");
        System.out.println("\n\t\t\t\t\t\t\tMenu Principal");
        System.out.println("\t\t\t0- Visualizar dados COVID19");
        System.out.println("\t\t\t1- Análise Comparativa dados COVID19");
        System.out.println("\t\t\t2- Previsão");
        System.out.println("\t\t\t8- Submeter novo ficheiro");
        System.out.println("\t\t\t9- Sair");
        System.out.println("\tEscolha uma opção:");
        System.out.println("######################################################################################");
        escolha = ler.nextInt();


        switch (escolha) {
            case 0:
                System.out.println("\t\t\tVisualizar dados COVID19");
                menuEscolhaDados(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, escolha, valicaotipoFicheiro);
                //colocar nome do metodo visualizar
                break;
            case 1:
                System.out.println("\t\t\tAnálise Comparativa dados COVID19");
                menuEscolhaDados(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, escolha, valicaotipoFicheiro);

                break;
            case 2:
                System.out.println("\t\t\tPrevisão");
                ler.nextLine();
                System.out.println("\tEscreva o nome do ficheiro contendo a matriz de transição:");
                String matrizDeInput = ler.nextLine();
                double[][] matrizTransicao = lerMatrizTransicao(matrizDeInput);  // criar matriz de transição

                cadeiasDeMarkov(dadosCasosDiarios, datasCasosDiarios, matrizTransicao);
                metodoDeCrout(dadosCasosDiarios, matrizTransicao);

                break;
            case 8:
                ler.nextLine();
                System.out.println("Escreva o nome do ficheiro");
                String fileName = ler.nextLine();
                System.out.println(fileName);
                File ficheiro = new File(fileName);
                verificarFicheiro(ficheiro);
                lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);


                break;
            case 9:
                metodoSair();
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 0, 1, 2, 8 ou 9.");

                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);

        }
    }
    // ------------------------------------menuEscolhaDados-------------------------------------------------------------
    /*
    opçao 0 = visualizar
    opçao 1 = analisar
     */

    public static void menuEscolhaDados(String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int opcao, int[] valicaotipoFicheiro) throws ParseException, IOException, InterruptedException {
        int dados;
        metodoEnter();
        limparEcra();
        System.out.printf("######################################################################################");
        System.out.println("\n\t\t\t\t\t\t\tMenu escolha tipo de dados");
        System.out.println("\t\t\t0- Novos casos COVID19");
        System.out.println("\t\t\t1- Casos totais COVID19");
        System.out.println("\t\t\t7- Retornar ao Menu Principal");
        System.out.println("\t\t\t8- Submeter novo ficheiro");
        System.out.println("\t\t\t9- Sair");
        System.out.println("\t Escolha uma opção:");
        System.out.println("######################################################################################");
        dados = ler.nextInt();


        switch (dados) {
            case 0:
                if (opcao == 0) {
                    if (valicaotipoFicheiro[0] == 1) {
                        System.out.println("\t\t\tVisualizar Novos Casos COVID19");
                        menuEspacoTemporal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, dados, opcao, valicaotipoFicheiro);
                    } else {
                        System.out.println("Tipo de ficheiro Invalido...por favor carregue o ficheiro necessario");
                        menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
                    }
                } else {
                    if (valicaotipoFicheiro[1] == 1) {
                        System.out.println("Tipo de ficheiro Invalido...");
                        menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
                    } else {
                        System.out.println("\t\t\tAnalisar Novos Casos COVID19");
                        menuParametroVisualizarNovosCasos(false, false, false, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, 0, opcao, valicaotipoFicheiro);
                    }

                    //    /*
                    //opçao 0 = visualizar
                    //opçao 1 = analisar
                    //
                    //temp = 0 diario
                    //*/
                    //


                }
// criar validaçao ficheiro carregado
                break;
            case 1:
                if (opcao == 0) {
                    System.out.println("\t\t\tVisualizar Casos Totais COVID19");
                    menuEspacoTemporal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, dados, opcao, valicaotipoFicheiro);
                } else {
                    System.out.println("\t\t\tAnalisar Casos Totais COVID19");
                    menuParametroVisualizarCasosTotais(false, false, false, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, 0, opcao, valicaotipoFicheiro);
                }
                break;
            case 7:
                System.out.println("\t\t\tRetornar ao Menu Principal");
                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
            case 8:
                ler.nextLine();
                System.out.println("Escreva o nome do ficheiro");
                String fileName;
                fileName = ler.nextLine();
                File ficheiro = new File(fileName);
                verificarFicheiro(ficheiro);
                lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
                menuEscolhaDados(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, valicaotipoFicheiro);

                break;
            case 9:
                metodoSair();
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 0, 1, 8 ou 9.");
                menuEscolhaDados(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, valicaotipoFicheiro);
        }
    }

    // ---------------------------------- menu Espaço Temporal----------------------------------------------------------
    /*
   opçao 0 = visualizar
   opçao 1 = analisar
   dados 0 = Novos casos COVID19
   dados 1 = Casos totais COVID19
    */


    public static void menuEspacoTemporal(String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int dados, int opcao, int[] valicaotipoFicheiro) throws ParseException, IOException, InterruptedException {
        metodoEnter();
        limparEcra();
        int espacoTemporal;
        System.out.printf("######################################################################################");
        System.out.println("\n\t\t\t\t\t\t\tMenu Resolução Temporal");
        System.out.println("\t\t\t0- Diário");
        System.out.println("\t\t\t1- Semanal");
        System.out.println("\t\t\t2- Mensal");
        System.out.println("\t\t\t7- Retornar ao Menu Principal");
        System.out.println("\t\t\t8- Submeter novo ficheiro");
        System.out.println("\t\t\t9- Sair");
        System.out.println("\tEscolha uma opção:");
        System.out.println("######################################################################################");

        espacoTemporal = ler.nextInt();

        boolean opcaoDiario = false;
        boolean opcaoSemanal = false;
        boolean opcaoMensal = false;
        switch (espacoTemporal) {
            case 0:
                opcaoMensal = false;
                opcaoSemanal = false;
                opcaoDiario = true;
                System.out.println("\t\t\t Resolução temporal diária");
                if (opcao == 0) {
                    if (dados == 0) {
                        menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosAcumulado, titulosCasosDiarios, espacoTemporal, opcao, valicaotipoFicheiro);
                    } else {
                        menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, espacoTemporal, opcao, valicaotipoFicheiro);
                    }
                } else {
                    if (dados == 0) {
                        menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosAcumulado, titulosCasosDiarios, espacoTemporal, opcao, valicaotipoFicheiro);
                    } else {
                        menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, espacoTemporal, opcao, valicaotipoFicheiro);
                    }
                }

                break;
            case 1:

                opcaoDiario = false;
                opcaoMensal = false;
                opcaoSemanal = true;
                System.out.println("\t\t\t Resolução temporal semanal");
                if (opcao == 0) {
                    if (dados == 0) {
                        menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosAcumulado, titulosCasosDiarios, espacoTemporal, opcao, valicaotipoFicheiro);
                    } else {
                        menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, espacoTemporal, opcao, valicaotipoFicheiro);
                    }
                }

                break;
            case 2:
                System.out.println("\t\t\t Resolução temporal mensal");
                opcaoSemanal = false;
                opcaoDiario = false;
                opcaoMensal = true;
                if (opcao == 0) {
                    if (dados == 0) {
                        menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosAcumulado, titulosCasosDiarios, espacoTemporal, opcao, valicaotipoFicheiro);
                    } else {
                        menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, espacoTemporal, opcao, valicaotipoFicheiro);
                    }
                }
                break;
            case 7:
                System.out.println("\t\t\tRetornar ao Menu Principal");
                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
                opcaoDiario = false;
                opcaoSemanal = false;
                opcaoMensal = false;
                break;
            case 8:
                ler.nextLine();
                System.out.println("\t\t\tEscreva o nome do ficheiro");
                String fileName = ler.nextLine();
                File ficheiro = new File(fileName);
                verificarFicheiro(ficheiro);
                lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
                menuEspacoTemporal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, dados, opcao, valicaotipoFicheiro);


                break;
            case 9:
                metodoSair();
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 0, 1, 2, 7, 8 ou 9");

                menuEspacoTemporal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, dados, opcao, valicaotipoFicheiro);

        }

    }

    // ----------------------------------  menu parametro VISUALIZAR----------------------------------------------------

    /*
opçao 0 = visualizar
opçao 1 = analisar
temp = 0 diario
 */

    public static void menuParametroVisualizarNovosCasos(boolean opcaoDiario, boolean opcaoSemanal, boolean opcaoMensal, String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int temp, int opcao, int[] valicaotipoFicheiro) throws
            ParseException, IOException, InterruptedException {
        metodoEnter();
        limparEcra();
        System.out.printf("######################################################################################");
        System.out.println("\n\t\t\t\t\t\t\tMenu Parametro a Análisar");
        System.out.println("\t\t\t1- Novos Casos");
        System.out.println("\t\t\t2- Novas Hospitalizações");
        System.out.println("\t\t\t3- Novos Internados UCI");
        System.out.println("\t\t\t4- Óbitos Diários");
        System.out.println("\t\t\t5- Todos os anteriores");
        System.out.println("\t\t\t7- Retornar ao Menu Principal");
        System.out.println("\t\t\t8- Submeter novo ficheiro");
        System.out.println("\t\t\t9- Sair");
        System.out.println("######################################################################################");

        float mediaParametro, desvioPadrao;
        int opcaoParametro;
        float[][] mediaTodos;
        float[][] desvioPadraoTodos;

        opcaoParametro = ler.nextInt();

        switch (opcaoParametro) {
            case 1:
                //Metodo Novos Casos
                System.out.println("\t\t\tNovos Casos");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, dadosAcumulado, titulosAcumulado, "novos", opcaoParametro); // INSERIR ARRAY DE NOVOS CASOS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {

                        metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, opcaoParametro, 1,false,"-1","-1","-1","-1");
                    }
                }
                break;
            case 2:
                //Método Novas Hospitalizações
                System.out.println("\t\t\tNovas Hospitalizações");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, dadosAcumulado, titulosAcumulado, "novos", opcaoParametro);// INSERIR ARRAY DE NOVOS CASOS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, opcaoParametro, 1,false,"-1","-1","-1","-1");
                    }
                }
                break;
            case 3:
                //Metodo Novos Internados
                System.out.println("\t\t\tNovos Internados UCI");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, dadosAcumulado, titulosAcumulado, "novos", opcaoParametro);// INSERIR ARRAY DE NOVOS CASOS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, opcaoParametro, 1,false,"-1","-1","-1","-1");
                    }
                }
                break;
            case 4:
                //Mortos Diários
                System.out.println("\t\t\tÓbitos Diarios");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, dadosAcumulado, titulosAcumulado, "novos", opcaoParametro); // INSERIR ARRAY DE NOVOS CASOS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, opcaoParametro, 1,false,"-1","-1","-1","-1");
                    }
                }
                break;
            case 5:
                //Todos os anteriores
                System.out.println("\t\t\tTodos");// tudo
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, dadosAcumulado, titulosAcumulado, "novos", opcaoParametro); // INSERIR ARRAY DE NOVOS CASOS
                } else {
                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasAcumulado, dadosAcumulado, titulosAcumulado, opcaoParametro, 1,false,"-1","-1","-1","-1");
                    }
                }
                break;
            case 7:
                System.out.println("\t\t\tRetornar ao Menu Principal");
                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
                break;
            case 8:
                ler.nextLine();
                //Metodo ler ficheiros
                System.out.println("\t\t\tEscreva o nome do ficheiro");
                String fileName = ler.nextLine();
                File ficheiro = new File(fileName);
                verificarFicheiro(ficheiro);
                lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
                menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, temp, valicaotipoFicheiro);
                break;
            case 9:
                //Metodo Sair
                metodoSair();
                System.out.println("\t\t\tSair");
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4, 5, 7, 8 a 9");
                menuParametroVisualizarNovosCasos(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, temp, valicaotipoFicheiro);
        }
    }

    // ----------------------------------  menu parametro VISUALIZAR CASOS TOTAIS---------------------------------------
    /*
opçao 0 = visualizar
opçao 1 = analisar
temp = 0 diario
*/
    public static void menuParametroVisualizarCasosTotais(boolean opcaoDiario, boolean opcaoSemanal,
                                                          boolean opcaoMensal, String[][] datasAcumulado, String[][] datasCasosDiarios, int[][] dadosAcumulado, int[][] dadosCasosDiarios, String[] titulosCasosDiarios, String[] titulosAcumulado, int temp, int opcao, int[] valicaotipoFicheiro) throws
            ParseException, IOException, InterruptedException {
        metodoEnter();
        limparEcra();
        System.out.printf("######################################################################################");
        System.out.println("\n\t\t\t\t\t\t\tMenu Parametro a Análisar");
        System.out.println("\t\t\t1- Total Infetados");
        System.out.println("\t\t\t2- Total Hospitalizações");
        System.out.println("\t\t\t3- Total Internados UCI");
        System.out.println("\t\t\t4- Total Óbitos Diários");
        System.out.println("\t\t\t5- Todos os anteriores");
        System.out.println("\t\t\t7- Retornar ao Menu Principal");
        System.out.println("\t\t\t8- Submeter novo ficheiro");
        System.out.println("\t\t\t9- Sair");
        System.out.println("######################################################################################");

        int opcaoParametro;
        float mediaParametro, desvioPadrao;
        float[][] mediaTodos;
        float[][] desvioPadraoTodos;

        opcaoParametro = ler.nextInt();

        switch (opcaoParametro) {
            case 1:
                System.out.println("\t\t\tTotal Infetados:");
                //Metodo Novos Casos
                if (opcao == 0) { // VISUALIZAR
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, "totais", opcaoParametro);
                } else {

                    if (temp == 0) { // ANALISAR

                        metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, opcaoParametro, 2,false,"-1","-1","-1","-1");
                        //mediaParametro = calculoMedia(dadosCasosDiarios, opcaoParametro);
                        //desvioPadrao = calculoDesvioPadrao(dadosCasosDiarios, opcaoParametro, mediaParametro);

                    }
                }

                break;

            case 2:
                //Método Novas Hospitalizações
                System.out.println("\t\t\tTotal Hospitalizações:");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, "totais", opcaoParametro); // DADOS CASOS TOTAIS
                } else {

                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, opcaoParametro, 2,false,"-1","-1","-1","-1");
                        // mediaParametro = calculoMedia(dadosCasosDiarios, opcaoParametro);
                        //desvioPadrao = calculoDesvioPadrao(dadosCasosDiarios, opcaoParametro, mediaParametro);

                    }
                }
                break;
            case 3:
                //Metodo Novos Internados
                System.out.println("\t\t\tTotal Internados UCI:");

                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, "totais", opcaoParametro); // DADOS CASOS TOTAIS
                } else {

                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, opcaoParametro, 2,false,"-1","-1","-1","-1");
                        // mediaParametro = calculoMedia(dadosCasosDiarios, opcaoParametro);
                        //desvioPadrao = calculoDesvioPadrao(dadosCasosDiarios, opcaoParametro, mediaParametro);

                    }
                }
                break;
            case 4:
                //Mortos Diários
                System.out.println("\t\t\tTotal Óbitos Diários:");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, "totais", opcaoParametro); // DADOS CASOS TOTAIS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {

                        metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, opcaoParametro, 2,false,"-1","-1","-1","-1");
                        // mediaParametro = calculoMedia(dadosCasosDiarios, opcaoParametro);
                        // desvioPadrao = calculoDesvioPadrao(dadosCasosDiarios, opcaoParametro, mediaParametro);

                    }
                }
                break;
            case 5:
                //Todos os anteriores
                System.out.println("\t\t\tTodos os anteriores;");
                if (opcao == 0) {
                    metodoVizualizarExibir(opcaoDiario, opcaoSemanal, opcaoMensal, datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, "totais", opcaoParametro); // DADOS CASOS TOTAIS
                } else {
                    //------INCLUIR METODO COMPARAR ---
                    if (temp == 0) {
                        metodoAnaliseComparativaTotaisENovosCasos(datasCasosDiarios, dadosCasosDiarios, titulosCasosDiarios, opcaoParametro, 2,false,"-1","-1","-1","-1");
                        // mediaTodos = calculoMediaTodosParametros(dadosCasosDiarios);
                        //desvioPadraoTodos = calculoDesvioPadraoTodosParametros(dadosCasosDiarios, mediaTodos);

                    }
                }
                break;
            case 7:
                System.out.println("\t\t\tRetornar ao Menu Principal");
                menuPrincipal(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, valicaotipoFicheiro);
                break;
            case 8:
                ler.nextLine();
                System.out.println("\t\t\tEscreva o nome do ficheiro");
                String fileName = ler.nextLine();
                File ficheiro = new File(fileName);
                verificarFicheiro(ficheiro);
                lerFicheiro(datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, fileName, valicaotipoFicheiro);
                menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, temp, valicaotipoFicheiro);
                break;
            case 9:
                //Metodo Sair
                metodoSair();
                System.out.println("\t\t\tSair");
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4, 5, 7, 8 a 9");
                menuParametroVisualizarCasosTotais(opcaoDiario, opcaoSemanal, opcaoMensal, datasAcumulado, datasCasosDiarios, dadosAcumulado, dadosCasosDiarios, titulosCasosDiarios, titulosAcumulado, opcao, temp, valicaotipoFicheiro);
        }
    }
    //-----------------------------------calculo da média-------------------------------------------
    public static float calculoMedia(int[][] dadosCovid, int temp) {
        int sum = 0, count = 0;
        int sizeL = dadosCovid.length;
        float media;
        for (int k = 0; k < sizeL - 1; k++) {
            sum = sum + dadosCovid[k + 1][temp];
            count++;
        }
        media = (float) (sum / count);
        return media;
    }
    //-----------------------------------desvio padrao-------------------------------------------
    public static float calculoDesvioPadrao(int[][] dadosCovid, int temp, float media) {
        int sum = 0, count = 0;
        float desvioPadrao = 0;
        int sizeL = dadosCovid.length;

        for (int k = 0; k < sizeL; k++) {
            sum = (int)(sum + Math.pow(dadosCovid[k][temp] - media,2));
            count++;
        }
        desvioPadrao = (float) Math.pow(((float)sum / count), 0.5);
        return desvioPadrao;
    }
    // ----------------------------------  validar datas  ---------------------------------------------------------
    public static boolean validacaoDatas(int linhaInicio, int linhaFim, String[][] datas) {
        String menorDataDisponivel = "";
        String maiorDataDisponivel = "";
        for (int i = 0; i < datas.length; i++) {
            if (datas[i][1] == "1") {
                menorDataDisponivel = datas[i][0];
                i = datas.length;
            }
        }
        for (int i = datas.length - 1; i > 0; i--) {
            if (datas[i][1] == "1") {
                maiorDataDisponivel = datas[i][0];
                i = 0;
            }
        }
        if (linhaInicio == -1) {
            System.out.println("Data Inicial Invalida!");
            System.out.printf("Intervalo de datas disponivel no ficheiro : %s até %s\n", menorDataDisponivel, maiorDataDisponivel);
            return false;
        } else if (linhaFim == -1) {
            System.out.println("Data Final Invalida!");
            System.out.printf("Intervalo de datas disponivel no ficheiro : %s até %s\n", menorDataDisponivel, maiorDataDisponivel);
            return false;
        } else {
            System.out.println(" Intervalo de datas Valido...");
            return true;
        }
    }

    // ---------------------------------- metodoAnaliseComparativaNovosCasos  ---------------------------------------------------------
    public static int[][] metodoAnaliseComparativaTotaisENovosCasos(String[][] datasCovid, int[][] dadosCovid, String[] tituloCovid, int opcao, int fileType, boolean naoInterativo, String di1, String df1, String di2, String df2) throws IOException, ParseException, InterruptedException {
        // fileType = 1 Novos Casos;
        // fileType = 2 Casos Totais;

        float[] mediaMatrix = new float[20], desvioPadrao = new float[20];
        int numeroDeDias = 0;
        boolean dataValida1 = false;
        boolean dataValida2 = false;
        String data1Intervalo1 = "";
        String data1Intervalo2 = "";
        String data2Intervalo1 = "";
        String data2Intervalo2 = "";
        int linhaData1intervalo1 = -1;
        int linhaData2intervalo1 = -1;
        int linhaData1intervalo2 = -1;
        int linhaData2intervalo2 = -1;
        String[] titulosOutput = new String[6];
        String[] titulosDifereca = new String[6];
        if (fileType == 1) {
            titulosOutput[0] = "Data Intervalo";
            titulosOutput[1] = "Número de Novos Casos";
            titulosOutput[2] = "Número de Novas Hospitalizações";
            titulosOutput[3] = "Número de Novos Internados em UCI";
            titulosOutput[4] = "Número de Óbitos";
        } else {
            titulosOutput[0] = "Data Intervalo";
            titulosOutput[1] = "Número Total de Infetados";
            titulosOutput[2] = "Número Total de Hospitalizados";
            titulosOutput[3] = "Número Total de Internados em UCI";
            titulosOutput[4] = "Número Total de Óbitos";
        }
        //Descobrir Datas em modo nao interativo
        if (naoInterativo == true) {
            data1Intervalo1 = di1;
            data2Intervalo1 = df1;
            data1Intervalo2 = di2;
            data2Intervalo2 = df2;
            linhaData1intervalo1 = descobrirData(datasCovid, data1Intervalo1);
            linhaData2intervalo1 = descobrirData(datasCovid, data2Intervalo1);
            linhaData1intervalo2 = descobrirData(datasCovid, data1Intervalo2);
            linhaData2intervalo2 = descobrirData(datasCovid, data2Intervalo2);
            dataValida1 = validacaoDatas(linhaData1intervalo1, linhaData2intervalo1, datasCovid);
            dataValida2 = validacaoDatas(linhaData1intervalo2, linhaData2intervalo2, datasCovid);
            if (!dataValida1||!dataValida2) {
                System.out.println("datas Invalidas");
                metodoEnter();
                metodoSair();
            }
        } else {
            Scanner ler = new Scanner(System.in);
            System.out.println("Insira os intervalos de datas");
            System.out.println("Formato de data para Novos Casos = (yyyy-MM-dd)");
            System.out.println("Formato de data para Casos Totais = (dd-MM-yyyy)");
            //################################################
            //1ºintervalo
            //################################################
            System.out.println("Data de início 1º Intervalo:");
            data1Intervalo1 = ler.nextLine();
            linhaData1intervalo1 = descobrirData(datasCovid, data1Intervalo1);
            System.out.println("Data de fim 1º Intervalo:");
            data2Intervalo1 = ler.nextLine();
            linhaData2intervalo1 = descobrirData(datasCovid, data2Intervalo1);
            //valida datas 1 Intervalo
            dataValida1 = validacaoDatas(linhaData1intervalo1, linhaData2intervalo1, datasCovid);
            if (!dataValida1) {
                System.out.println("Intervalo inválido, programa irá terminar!");
                metodoSair();
            }
            //################################################
            //2ºintervalo
            //################################################
            System.out.println("Data de início 2º Intervalo:");
            data1Intervalo2 = ler.nextLine();
            linhaData1intervalo2 = descobrirData(datasCovid, data1Intervalo2);
            System.out.println("Data de fim 2º Intervalo:");
            data2Intervalo2 = ler.nextLine();
            linhaData2intervalo2 = descobrirData(datasCovid, data2Intervalo2);
            ler.nextLine();
            //valida datas 2 Intervalo
            dataValida2 = validacaoDatas(linhaData1intervalo2, linhaData2intervalo2, datasCovid);
            if (!dataValida2) {
                System.out.println("Intervalo inválido, programa irá terminar!");
                metodoSair();
            }
        }
        // validação do primeiro dia dos de cada intervalo para os acumulados, caso não exista dia anterior só é possivel fazer do dia a seguir
        if (fileType == 1) {
            if(datasCovid[linhaData1intervalo1-1][1].equals("0")){
                linhaData1intervalo1++;
                data1Intervalo1 = datasCovid[linhaData1intervalo1][0];
                System.out.printf("Não é possivel obter dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n",datasCovid[linhaData1intervalo1-1][0],datasCovid[linhaData1intervalo1][0]);
            }
            if(datasCovid[linhaData1intervalo2-1][1].equals("0")){
                linhaData1intervalo2++;
                data1Intervalo2 = datasCovid[linhaData1intervalo2][0];
                System.out.printf("Não é possivel obter dados para a data: %s, logo só é possivel apresentar do dia %s em diante.\n",datasCovid[linhaData1intervalo2-1][0],datasCovid[linhaData1intervalo2][0]);
            }
        }
        /*
        Encontrar menor numero de dias
         */
        int menorIntervaloLenght = 2;
        int numerDias1intervalo = (linhaData2intervalo1 - linhaData1intervalo1) + 1;
        int numerDias2intervalo = (linhaData2intervalo2 - linhaData1intervalo2) + 1;
        int maxDias = numerDias1intervalo;
        int minDias = numerDias2intervalo;
        if (maxDias < numerDias2intervalo) {
            menorIntervaloLenght = 1;
            minDias = numerDias1intervalo;
        }

        /*
        Valores diarios dos 2 intervalos
        */
        int[][] resultado1Intervalo;
        int[][] resultado2Intervalo;

        if (fileType == 1) { // novos
            resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
            resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
        } else {
            resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
            resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
        }
        /*
        Matrix para o resultado do metodo
                */
        int totalLinhas = resultado1Intervalo.length;
        int linhaInicio = linhaData1intervalo1;
        int linhaFim = linhaInicio + minDias;
        if (menorIntervaloLenght == 2) {
            totalLinhas = resultado2Intervalo.length;
            linhaInicio = linhaData1intervalo2;
            linhaFim = linhaInicio + minDias;
        }
        int numColunasDiarias = resultado1Intervalo[0].length;
        int totalColunas = (numColunasDiarias * 3) - 1; //por definir
        int[][] matrixAnaliseComparativaDadosCovid = new int[totalLinhas][totalColunas];
        // Preencher matriz com valor -1
        for (int i = 0; i < matrixAnaliseComparativaDadosCovid.length; i++) {
            for (int j = 0; j < matrixAnaliseComparativaDadosCovid[0].length; j++) {
                matrixAnaliseComparativaDadosCovid[i][j] = -1;
            }
        }
        for (int i = 0; i < 20; i++) {
            mediaMatrix[i] = -1;
            desvioPadrao[i] = -1;
        }
          /*
        Prencher Matrix de resultado com os valores diarios
        para o mesmo numero de linhas do menor intervalo
         */

        switch (opcao) {
            case 5:
                for (int j = 0; j < numColunasDiarias; j++) {
                    System.out.printf(" %33s |", titulosOutput[j]);
                }
                for (int j = 0; j < numColunasDiarias; j++) {
                    System.out.printf(" %33s |", titulosOutput[j]);
                }
                for (int j = 1; j < numColunasDiarias; j++) {
                    System.out.printf(" %33s Diferença |", titulosOutput[j]);
                }
                System.out.println();
                if (fileType == 1) { // novos
                    resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                } else {
                    resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                }
                for (int coluna = 0; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][coluna] = resultado1Intervalo[linha][coluna];
                        matrixAnaliseComparativaDadosCovid[linha][coluna + 5] = resultado2Intervalo[linha][coluna];
                    }
                }
                for (int coluna = 1; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][coluna + 9] = Math.abs(matrixAnaliseComparativaDadosCovid[linha][coluna] - matrixAnaliseComparativaDadosCovid[linha][coluna + 5]);
                    }
                }
                for (int linhas = 0; linhas < minDias; linhas++) {
                    for (int colunas = 0; colunas < 14; colunas++) {
                        if (colunas == 0 || colunas == 5) {
                            System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][colunas]][0]);
                        }else if(colunas == 10 || colunas == 11 || colunas == 12|| colunas == 13){
                            System.out.printf(" %43d |", matrixAnaliseComparativaDadosCovid[linhas][colunas]);
                        }else{
                            System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][colunas]);
                        }
                    }
                    System.out.println();
                }
                for (int colunas = 0; colunas < 14; colunas++) {
                    if (colunas == 5 || colunas == 0){
                        mediaMatrix[colunas] = -1;
                        desvioPadrao[colunas] = -1;
                    }else{
                        mediaMatrix[colunas] = calculoMedia(matrixAnaliseComparativaDadosCovid, colunas);
                        desvioPadrao[colunas] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, colunas, mediaMatrix[colunas]);
                    }
                }
                System.out.print("                             Média |");
                for (int i = 1; i < mediaMatrix.length; i++) {
                    if(i==10 || i==11 || i==12 || i==13){
                        System.out.printf(" %43.4f |", mediaMatrix[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if(mediaMatrix[i] != -1){
                        System.out.printf(" %33.4f |", mediaMatrix[i]);
                    }
                }
                System.out.println();
                System.out.print("                     Desvio Padrão |");
                for (int i = 1; i < desvioPadrao.length; i++) {
                    if(i==10 || i==11 || i==12 || i==13){
                        System.out.printf(" %43.4f |", desvioPadrao[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if (desvioPadrao[i] != -1){
                        System.out.printf(" %33.4f |", desvioPadrao[i]);
                    }
                }
                break;
            case 4:
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[4]);
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[4]);
                System.out.printf(" Diferença %33s |", titulosOutput[4]);
                System.out.println();
                if (fileType == 1) { // novos
                    resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                } else {
                    resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                }

                for (int linha = 0; linha < minDias; linha++) {
                    matrixAnaliseComparativaDadosCovid[linha][4] = resultado1Intervalo[linha][4];
                    matrixAnaliseComparativaDadosCovid[linha][9] = resultado2Intervalo[linha][4];
                    matrixAnaliseComparativaDadosCovid[linha][0] = resultado1Intervalo[linha][0];
                    matrixAnaliseComparativaDadosCovid[linha][5] = resultado2Intervalo[linha][0];
                }
                for (int coluna = 1; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][13] = Math.abs(matrixAnaliseComparativaDadosCovid[linha][4] - matrixAnaliseComparativaDadosCovid[linha][9]);
                    }
                }
                for (int linhas = 0; linhas < minDias; linhas++) {
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][0]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][4]);
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][5]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][9]);
                    System.out.printf("           %33d |", matrixAnaliseComparativaDadosCovid[linhas][13]);
                    System.out.println();
                }
                mediaMatrix[4] = calculoMedia(matrixAnaliseComparativaDadosCovid, 4);
                desvioPadrao[4] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 4, mediaMatrix[4]);
                mediaMatrix[9] = calculoMedia(matrixAnaliseComparativaDadosCovid, 9);
                desvioPadrao[9] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 9, mediaMatrix[9]);
                mediaMatrix[13] = calculoMedia(matrixAnaliseComparativaDadosCovid, 13);
                desvioPadrao[13] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 13, mediaMatrix[13]);
                System.out.print("                             Média |");
                for (int i = 1; i < mediaMatrix.length; i++) {
                    if(i==13){
                        System.out.printf(" %43.4f |", mediaMatrix[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if(mediaMatrix[i] != -1){
                        System.out.printf(" %33.4f |", mediaMatrix[i]);
                    }
                }
                System.out.println();
                System.out.print("                     Desvio Padrão |");
                for (int i = 1; i < desvioPadrao.length; i++) {
                    if(i==13){
                        System.out.printf(" %43.4f |", desvioPadrao[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if (desvioPadrao[i] != -1){
                        System.out.printf(" %33.4f |", desvioPadrao[i]);
                    }
                }
                break;
            case 3:
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[3]);
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[3]);
                System.out.printf(" Diferença %33s |", titulosOutput[3]);
                System.out.println();
                if (fileType == 1) { // novos
                    resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                } else {
                    resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                }
                for (int linha = 0; linha < minDias; linha++) {
                    matrixAnaliseComparativaDadosCovid[linha][3] = resultado1Intervalo[linha][3];
                    matrixAnaliseComparativaDadosCovid[linha][8] = resultado2Intervalo[linha][3];
                    matrixAnaliseComparativaDadosCovid[linha][0] = resultado1Intervalo[linha][0];
                    matrixAnaliseComparativaDadosCovid[linha][5] = resultado2Intervalo[linha][0];
                }
                for (int coluna = 1; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][12] = Math.abs(matrixAnaliseComparativaDadosCovid[linha][3] - matrixAnaliseComparativaDadosCovid[linha][8]);
                    }
                }
                for (int linhas = 0; linhas < minDias; linhas++) {
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][0]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][3]);
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][5]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][8]);
                    System.out.printf("           %33d |", matrixAnaliseComparativaDadosCovid[linhas][12]);
                    System.out.println();
                }
                mediaMatrix[3] = calculoMedia(matrixAnaliseComparativaDadosCovid, 3);
                desvioPadrao[3] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 3, mediaMatrix[3]);
                mediaMatrix[8] = calculoMedia(matrixAnaliseComparativaDadosCovid, 8);
                desvioPadrao[8] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 8, mediaMatrix[8]);
                mediaMatrix[12] = calculoMedia(matrixAnaliseComparativaDadosCovid, 12);
                desvioPadrao[12] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 12, mediaMatrix[12]);
                System.out.print("                             Média |");
                for (int i = 1; i < mediaMatrix.length; i++) {
                    if(i==12){
                        System.out.printf(" %43.4f |", mediaMatrix[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if(mediaMatrix[i] != -1){
                        System.out.printf(" %33.4f |", mediaMatrix[i]);
                    }
                }
                System.out.println();
                System.out.print("                     Desvio Padrão |");
                for (int i = 1; i < desvioPadrao.length; i++) {
                    if(i==12){
                        System.out.printf(" %43.4f |", desvioPadrao[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if (desvioPadrao[i] != -1){
                        System.out.printf(" %33.4f |", desvioPadrao[i]);
                    }
                }
                break;
            case 2:
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[2]);
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[2]);
                System.out.printf(" Diferença %33s |", titulosOutput[2]);
                System.out.println();
                if (fileType == 1) { // novos
                    resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                } else {
                    resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                }
                for (int linha = 0; linha < minDias; linha++) {
                    matrixAnaliseComparativaDadosCovid[linha][2] = resultado1Intervalo[linha][2];
                    matrixAnaliseComparativaDadosCovid[linha][7] = resultado2Intervalo[linha][2];
                    matrixAnaliseComparativaDadosCovid[linha][0] = resultado1Intervalo[linha][0];
                    matrixAnaliseComparativaDadosCovid[linha][5] = resultado2Intervalo[linha][0];
                }
                for (int coluna = 1; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][11] = Math.abs(matrixAnaliseComparativaDadosCovid[linha][2] - matrixAnaliseComparativaDadosCovid[linha][7]);
                    }
                }
                for (int linhas = 0; linhas < minDias; linhas++) {
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][0]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][2]);
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][5]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][7]);
                    System.out.printf("           %33d |", matrixAnaliseComparativaDadosCovid[linhas][11]);
                    System.out.println();
                }
                mediaMatrix[2] = calculoMedia(matrixAnaliseComparativaDadosCovid, 2);
                desvioPadrao[2] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 2, mediaMatrix[2]);
                mediaMatrix[7] = calculoMedia(matrixAnaliseComparativaDadosCovid, 7);
                desvioPadrao[7] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 7, mediaMatrix[7]);
                mediaMatrix[11] = calculoMedia(matrixAnaliseComparativaDadosCovid, 11);
                desvioPadrao[11] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 11, mediaMatrix[11]);
                System.out.print("                             Média |");
                for (int i = 1; i < mediaMatrix.length; i++) {
                    if(i==11){
                        System.out.printf(" %43.4f |", mediaMatrix[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if(mediaMatrix[i] != -1){
                        System.out.printf(" %33.4f |", mediaMatrix[i]);
                    }
                }
                System.out.println();
                System.out.print("                     Desvio Padrão |");
                for (int i = 1; i < desvioPadrao.length; i++) {
                    if(i==11){
                        System.out.printf(" %43.4f |", desvioPadrao[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if (desvioPadrao[i] != -1){
                        System.out.printf(" %33.4f |", desvioPadrao[i]);
                    }
                }
                break;
            case 1:
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[1]);
                System.out.printf(" %33s |", titulosOutput[0]);
                System.out.printf(" %33s |", titulosOutput[1]);
                System.out.printf(" Diferença %33s |", titulosOutput[1]);
                System.out.println();
                if (fileType == 1) { // novos
                    resultado1Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarAcumuladoDiarios(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                } else {
                    resultado1Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo1, datasCovid[linhaData1intervalo1 + minDias][0], tituloCovid, opcao, true);
                    resultado2Intervalo = mostrarCasosTotaisDiario(datasCovid, dadosCovid, data1Intervalo2, datasCovid[linhaData1intervalo2 + minDias][0], tituloCovid, opcao, true);
                }

                for (int linha = 0; linha < minDias; linha++) {
                    matrixAnaliseComparativaDadosCovid[linha][1] = resultado1Intervalo[linha][1];
                    matrixAnaliseComparativaDadosCovid[linha][6] = resultado2Intervalo[linha][1];
                    matrixAnaliseComparativaDadosCovid[linha][0] = resultado1Intervalo[linha][0];
                    matrixAnaliseComparativaDadosCovid[linha][5] = resultado2Intervalo[linha][0];
                }
                for (int coluna = 1; coluna < numColunasDiarias; coluna++) {
                    for (int linha = 0; linha < minDias; linha++) {
                        matrixAnaliseComparativaDadosCovid[linha][10] = Math.abs(matrixAnaliseComparativaDadosCovid[linha][1] - matrixAnaliseComparativaDadosCovid[linha][6]);
                    }
                }
                for (int linhas = 0; linhas < minDias; linhas++) {
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][0]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][1]);
                    System.out.printf(" %33s |", datasCovid[matrixAnaliseComparativaDadosCovid[linhas][5]][0]);
                    System.out.printf(" %33d |", matrixAnaliseComparativaDadosCovid[linhas][6]);
                    System.out.printf("           %33d |", matrixAnaliseComparativaDadosCovid[linhas][10]);
                    System.out.println();
                }
                mediaMatrix[1] = calculoMedia(matrixAnaliseComparativaDadosCovid, 1);
                desvioPadrao[1] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 1, mediaMatrix[1]);
                mediaMatrix[6] = calculoMedia(matrixAnaliseComparativaDadosCovid, 6);
                desvioPadrao[6] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 6, mediaMatrix[6]);
                mediaMatrix[10] = calculoMedia(matrixAnaliseComparativaDadosCovid, 10);
                desvioPadrao[10] = calculoDesvioPadrao(matrixAnaliseComparativaDadosCovid, 10, mediaMatrix[10]);
                System.out.print("                             Média |");
                for (int i = 1; i < mediaMatrix.length; i++) {
                    if(i==10){
                        System.out.printf(" %43.4f |", mediaMatrix[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if(mediaMatrix[i] != -1){
                        System.out.printf(" %33.4f |", mediaMatrix[i]);
                    }
                }
                System.out.println();
                System.out.print("                     Desvio Padrão |");
                for (int i = 1; i < desvioPadrao.length; i++) {
                    if(i==11){
                        System.out.printf(" %43.4f |", desvioPadrao[i]);
                    }else if(i==5){
                        System.out.print("                                   |");
                    }else if (desvioPadrao[i] != -1){
                        System.out.printf(" %33.4f |", desvioPadrao[i]);
                    }
                }
                break;
            default:
                System.out.println("\t\t\tValor inválido. Escolha uma opção entre 1, 2, 3, 4 ou 5.");
        }
        if(naoInterativo==false) {
            questionarFicheiro(matrixAnaliseComparativaDadosCovid, titulosOutput, 1, mediaMatrix, desvioPadrao, true);
        }
        return matrixAnaliseComparativaDadosCovid;
    }

    // ----------------------------------  Visualizar ---------------------------------------------------------
    public static int[][] metodoVizualizarExibir(boolean opcaoDiario, boolean opcaoSemanal,
                                                 boolean opcaoMensal, String[][] datas, int[][] dadosCovid, String[] titulo, String tipo, int parametro) throws ParseException, IOException, InterruptedException {
        Scanner ler = new Scanner(System.in);
        String guardarEmFicheiro = "";
        boolean dataValida = false;
        String dataInicio;
        if (tipo == "totais") {
            dataInicio = pedirIntervalosDI(2);
        } else {
            dataInicio = pedirIntervalosDI(1);
        }
        String dataFim = pedirIntervalosDF();
        int linhaInicio = descobrirData(datas, dataInicio);
        int linhaFim = descobrirData(datas, dataFim);
        dataValida = validacaoDatas(linhaInicio, linhaFim, datas);

        if (dataValida == false) {
            metodoSair();
        }
        int[][] resultadosAnalise = new int[linhaFim - linhaInicio][9];
        if (dataValida == true) {
            if (opcaoDiario == true) {
                if (tipo == "totais") {
                    mostrarCasosTotaisDiario(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                } else {
                    mostrarAcumuladoDiarios(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                }
            } else if (opcaoSemanal == true) {
                if (tipo == "totais") {
                    mostrarCasosTotaisSemanal(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                } else {
                    mostrarAcumuladoSemanal(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                }
            } else {
                if (tipo == "totais") {
                    mostrarCasosTotaisMensal(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                } else {
                    mostrarAcumuladoMensal(datas, dadosCovid, dataInicio, dataFim, titulo, parametro, false);
                }
            }
        }
        return resultadosAnalise;
    }

    // ----------------------------------  pedir intervalos ---------------------------------------------------------
    private static String pedirIntervalosDI(int tipoFicheiro) throws IOException, InterruptedException {
        //1 = acumulados 2=totais
        if (tipoFicheiro == 1) {
            System.out.println("Insira o intervalo de datas (2020-04-01)");
        } else {
            System.out.println("Insira o intervalo de datas (01-04-2020)");
        }

        metodoEnter();
        System.out.println("Insira data de inicio:");
        Scanner ler = new Scanner(System.in);
        String intervaloDI = ler.nextLine();
        return intervaloDI;
    }

    private static String pedirIntervalosDF() throws IOException, InterruptedException {
        System.out.println("Insira data de fim:");
        Scanner ler = new Scanner(System.in);
        String intervaloDF = ler.nextLine();
        metodoEnter();
        limparEcra();
        return intervaloDF;
    }

    public static void questionarFicheiro(int[][] resultadoD, String[] titulo, int tipoFicheiroGuardar, float[] valor1, float[] valor2, boolean comparativa) throws
            IOException, InterruptedException {
        // tipoFicheiroGuardar: ARRAY 2D1 - csv 2 - txt
        String guardarEmFicheiro = "";
        int contador = 0;
        while (!(guardarEmFicheiro.toLowerCase().equals("nao") || guardarEmFicheiro.toLowerCase().equals("sim"))) {
            if (contador != 0) {
                System.out.println("Resposta Invalida...Por favor insira uma das duas opcções (sim/nao) ");
            }
            System.out.println("Deseja Guardar os Resultados num ficheiro?(sim/nao)");
            contador++;
            guardarEmFicheiro = ler.nextLine();
        }
        if (guardarEmFicheiro.toLowerCase().equals("sim")) {
            System.out.println("Insira o nome de ficheiro desejado");
            String nomeDoFicheiro = ler.nextLine();
            if (tipoFicheiroGuardar == 1) {
                guardarEmFicheiroInt2DCSV(resultadoD, valor1, valor2, titulo, nomeDoFicheiro, comparativa);
            } else if (tipoFicheiroGuardar == 2) {
                guardarEmFicheiroInt2DTXT(resultadoD, titulo, nomeDoFicheiro);
            }
            System.out.println("\n\t\t\t\tFicheiro Guardado...");
            System.out.println("---------");
            System.out.println("|--------| ");
            System.out.println("|------  | ");
            System.out.println("|  saved | ");
            System.out.println("--------- ");
            metodoSair();
        } else {
            metodoSair();

        }

    }


    // ----------------------------------  metodoSair ---------------------------------------------------------

    private static void metodoSair() {

        System.out.println("Obrigado por usar a nossa app\n Copyrigth @ The Mighty Squad");
        System.exit(-1);

    }
    // ----------------------------------  metodos CSV ---------------------------------------------------------

    public static String guardarEmFicheiroInt2DCSV(int[][] dados, float[] valores1, float[] valores2, String[] titulo, String nomeFicheiro, boolean comparativa) throws
            IOException {
        String separador = "";
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd");
        String data = formatoData.format(new Date());
        String resultFile = nomeFicheiro + "_" + data + ".csv";

        File ficheiro = new File(resultFile);
        if (ficheiro.createNewFile()) {
            System.out.println("File created: " + ficheiro.getName());
        } else {
            System.out.println("File exists and it was updated: " + ficheiro.getName());
        }
        try {
            FileWriter myWriter = new FileWriter(ficheiro);
            PrintWriter escritor = new PrintWriter(myWriter);
            for (int i = 0; i < titulo.length; i++) {
                if (i == titulo.length - 1) {
                    escritor.printf("%s", titulo[i]);
                } else {
                    escritor.printf("%s,", titulo[i]);
                }
            }
            escritor.println();
            for (int i = 0; i < dados.length; i++) {
                escritor.println();
                for (int j = 0; j < dados[0].length; j++) {
                    if (dados[i][j] != -1) {
                        escritor.printf("%d,", dados[i][j]);
                    }
                }
            }
            escritor.println();
            if (comparativa) {
                System.out.printf("Media:\n");
                for (int i = 0; i < valores1.length; i++) {
                    escritor.printf("%.4f", valores1[i]);
                }
                escritor.println();
                System.out.printf("Desvio padrao:\n");
                for (int i = 0; i < valores2.length; i++) {
                    escritor.printf("%.4f", valores2[i]);
                }
                escritor.println();
            }
            escritor.println();
            escritor.println(String.format("%s", separador));
            escritor.println("Export Feito no dia :" + data);
            escritor.close();
        } catch (IOException e) {
        }
        return resultFile;
    }


    public static String guardarEmFicheiroInt2DTXT(int[][] dados, String[] titulo, String nomeFicheiro) throws
            IOException {
        String dadosImpressaotxt = "Resultados_Covid19_TesteTXT_2D";
        String separador = "";
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd");
        String data = formatoData.format(new Date());
        String resultFile = dadosImpressaotxt + "_" + nomeFicheiro + "_" + data + ".txt";

        File ficheiro = new File(resultFile);
        if (ficheiro.createNewFile()) {
            System.out.println("File created: " + ficheiro.getName());
        } else {
            System.out.println("File exists and it was updated: " + ficheiro.getName());
        }

        try {

            FileWriter myWriter = new FileWriter(ficheiro);
            PrintWriter escritor = new PrintWriter(myWriter);
            for (int i = 1; i < titulo.length; i++) {
                escritor.printf("%30s |", titulo[i]);


            }
            for (int i = 1; i < titulo.length; i++) {
                separador = separador + "----------------------------------------";
            }
            escritor.println();
            escritor.println(String.format("%s", separador));

            for (int i = 0; i < dados.length; i++) {

                for (int j = 0; j < dados[0].length; j++) {
                    escritor.printf("%30d |", dados[i][j]);
                }
                escritor.println();
            }

            escritor.println();
            escritor.println(String.format("%s", separador));
            escritor.println("Export Feito no dia :" + data);
            escritor.close();
        } catch (IOException e) {

        }
        return resultFile;
    }

    public static String guardarEmFicheiroInt1DTXT(int[] dados, String[] titulo, String nomeFicheiro) throws
            IOException {
        String dadosImpressaotxt = "Resultados_Covid19_TesteTXT_1D";
        String separador = "";
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd");
        String data = formatoData.format(new Date());
        String resultFile = dadosImpressaotxt + "_" + nomeFicheiro + "_" + data + ".txt";
        File ficheiro = new File(resultFile);
        if (ficheiro.createNewFile()) {
            System.out.println("File created: " + ficheiro.getName());
        } else {
            System.out.println("File exists and it was updated: " + ficheiro.getName());
        }
        try {
            FileWriter myWriter = new FileWriter(ficheiro);
            PrintWriter escritor = new PrintWriter(myWriter);
            for (int i = 0; i < titulo.length; i++) {
                escritor.printf("%30s |", titulo[i]);
            }
            for (int i = 1; i < titulo.length; i++) {
                separador = separador + "----------------------------------------";
            }
            escritor.println();
            escritor.println(String.format("%s", separador));
            for (int i = 0; i < dados.length; i++) {
                escritor.printf("%30d |", dados[i]);
            }


            escritor.println();
            escritor.println(String.format("%s", separador));
            escritor.println("Export Feito no dia :" + data);
            escritor.close();
        } catch (IOException e) {

        }
        return resultFile;
    }

    //########################################## Limpar Ecra ##############################################################
    public static void limparEcra() throws IOException, InterruptedException {
        if (System.getProperty("os.name").contains("Windows")) {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } else {
            Runtime.getRuntime().exec("clear");
        }
    }

//########################################## PREVISÃO ##############################################################

    // ------------------------------------- METODO DE MARKOV ------------------------------------------------------

//                                    **** SÓ FICHEIROS DOS TOTAIS *****

    public static void cadeiasDeMarkov(int[][] ficheiroDadosTotais, String[][] datas, double[][] matrizTransicao) throws ParseException {
        System.out.println("Insira o dia: ");
        String dataPretendida = ler.nextLine();
        String dataT = checkData(datas, dataPretendida); // descobrir a data mais perto


        int k = descobrirData(datas, dataPretendida) - descobrirData(datas, dataT); // determinar k, para ver numero de vezes a multiplicar matrizes

        double[][] vetorTransposto = matrizTransposta(dataT, ficheiroDadosTotais, datas, matrizTransicao); //criar vetor X transposto

        System.out.println("A previsão é: ");
        for (int i = 1; i <= k; i++) {
            double[][] vetorDados = multiplicacaoMatrizes(matrizTransicao, vetorTransposto);
            vetorTransposto = vetorDados;
        }
        for (int y = 0; y < 5; y++) {
            System.out.print(y + " " + vetorTransposto[y][0]);
            System.out.println();
        }
    }

    public static double[][] matrizTransposta(String dataMaisProxima, int[][] ficheiroDadosTotais, String[][] datas, double[][] matrizTransicao) {
        int sizeC = matrizTransicao.length;
        int sizeL = matrizTransicao[0].length;

        double[][] matrizXt = new double[sizeC][1];
        int linha = descobrirData(datas, dataMaisProxima);

        for (int i = 0; i < sizeL; i++) {
            matrizXt[i][0] = ficheiroDadosTotais[linha][i];
        }

        return matrizXt;
    }

    public static double[][] multiplicacaoMatrizes(double[][] matrizTransicao, double[][] vetorEstados) {
        int sizeL = vetorEstados.length; // numero de linhas do vetor x=5;
        int sizeC = 1; // numero de colunas do vetor x=1
        double[][] vetorTransicao = new double[sizeL][sizeC];
        double[][] vetorEstadosTransicao = new double[sizeL][sizeC];
        for (int i = 0; i < matrizTransicao.length; i++) {
            for (int j = 0; j < matrizTransicao.length; j++) {
                vetorTransicao[i][0] = (vetorEstadosTransicao[i][0] + matrizTransicao[i][j] * vetorEstados[j][0]);
                vetorEstadosTransicao[i][0] = vetorTransicao[i][0];
            }

        }

        return vetorEstadosTransicao;
    }

    public static double[][] lerMatrizTransicao(String file) throws FileNotFoundException {
        int lineCounter = 0, empty = 0;
        int colunCounter = 0, totalLines, count1;
        Scanner ler = new Scanner(new File(file));
// Vai contar o numero de linhas (incluindo as vazias) e colunas do ficheiro.
        while (ler.hasNextLine()) {
            String[] lines = ler.nextLine().trim().split("=");
            count1 = lines.length;
            lineCounter++;
            if (count1 > colunCounter) {
                colunCounter = count1;
            }
            if (count1 < 2) {
                empty++;
            }
        }

        ler.close();

        ler = new Scanner(new File(file));
//Vai obter o numero real de linhas, sem as linhas vazias (ENTER's)
        totalLines = lineCounter - empty;

        int count2 = 0;


        int sizeC = (int) Math.pow(totalLines, 0.5); // Leitura das colunas do array
        int sizeL = sizeC;
        //  System.out.println(sizeL+"    "+ sizeC);
        double[][] arraySaida = new double[sizeL][sizeC];
//Vai preencher a matriz de transição com os dados da 2ª coluna. Sempre que encontrar uma linha vazia, counter2++,
//o que significa que está na altura de preencher outra coluna.
        // while(ler.hasNextLine()){
        for (int i = 0; i < sizeL; i++) {
            for (int j = 0; j < sizeL; j++) {
                String[] line = ler.nextLine().trim().split("=");
                arraySaida[i][j] = Float.parseFloat(line[1]);

            }
            ler.nextLine();
        }
        // }

        ler.close();

        return arraySaida;
    }

        public static String checkData(String[][] datas, String dataPretendida) throws ParseException {
            int sizeL = datas[0].length;
            int linha, count = 1;
            String dataT = "";
            linha = descobrirData(datas, dataPretendida);

            if (linha != -1) {
                dataT = datas[linha - 1][0];
            } else if (linha == -1) {
                linha = descobrirDataParaEntradaDeFicheiros(datas, dataPretendida);
                while (!datas[linha][1].equals("1")) {
                    linha--;
                }
                dataT = datas[linha][0];
            }
            return dataT;
        }

    // ------------------------------------- METODO DE CROUT - DECOMPOSIÇÃO LU ------------------------------------------------------

    //                                    **** SÓ FICHEIROS DOS TOTAIS *****


    public static void metodoDeCrout(int[][] DadosTotais, double[][] matrizTransicao) {

        double[][] matrizQ = criarMatrizQ(matrizTransicao);
        double[][] matrizI = criarMatrizIdentidade(matrizQ);

        double[][] matrizN = criarMatrizNaoInversa(matrizQ, matrizI);




        double determinante = calcularDeterminante(matrizN);

        if (determinante == 0) {
            System.out.println("O determinante da matriz é igual a 0. Impossivel fazer a decomposição LU!");
            metodoSair();
        }

        int sizeL = matrizN[0].length;
        int sizeC = matrizN.length;
        double[][] matrizU = new double[sizeL][sizeC];
        double[][] matrizL = new double[sizeL][sizeC];

        decomposicaoLU(matrizN,matrizU,matrizL);

        double[][] inversaL = criarMatrizInversaL(matrizL);
        double[][] inversaU = criarMatrizInversaU(matrizU);


        double[][] matrizInversaN = multiplicacaoMatrizesQuadradas(inversaU, inversaL);
        int[] matrizLinhaUm = {1, 1, 1, 1};
        double[][] matrizM = obterMatrizM(matrizInversaN, matrizLinhaUm);
        System.out.println("MatrizM:");
        for (int x = 0; x < sizeL; x++) {
            for (int y = 0; y < sizeC; y++) {
                System.out.print(x+y + "  " + matrizM[x][y]);
            }
            System.out.println();
        }
    }

    public static double[][] obterMatrizM(double[][] matrizInversa, int[] matrizUm) {
        int sizeL = matrizInversa.length;
        int sizeC = matrizInversa[0].length;
        double[][] matrizM = new double[sizeL][sizeC];
        double[][] matrizFinal = new double[sizeL][sizeC];
        for (int i = 0; i < sizeC; i++) {
            for (int j = 0; j < sizeL; j++) {
                matrizM[0][i] = (matrizFinal[0][i] + matrizUm[i] * matrizInversa[j][i]);
                matrizFinal[0][i] = matrizM[0][1];
            }
        }


        return matrizFinal;
    }


    public static double[][] multiplicacaoMatrizesQuadradas(double[][] inversaU, double[][] inversaL) {
        int sizeL = inversaU.length;
        int sizeC = inversaU[0].length;

        double[][] matrizAuxiliar = new double[sizeL][sizeC];
        double[][] matrizFinal = new double[sizeL][sizeC];
        for (int i = 0; i < sizeL; i++) {
            for (int j = 0; j < sizeC; j++) {
                matrizAuxiliar[i][j] = (double) (matrizFinal[i][0] + inversaU[i][j] * inversaL[j][i]);
                matrizFinal[i][j] = matrizAuxiliar[i][j];
            }
        }
        return matrizFinal;
    }

    public static double[][] criarMatrizInversaL(double[][] matriz) {
        int lines = matriz[0].length;
        int columns = matriz.length;
        double[][] matrizInversaL = new double[lines][columns];
        double[][] matrizI = criarMatrizIdentidade(matriz);

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < lines; j++) {
                if (i == j) {
                    matrizInversaL[i][j] = matrizI[i][j] / matriz[i][j];
                }
                if (i > j) {
                    double varivelAuxiliar = 0;
                    for (int k = 0; k < i; ++k) {
                        matrizInversaL[i][j] = (matrizI[i][j] - matriz[i][k] * matrizInversaL[k][j]) + varivelAuxiliar;
                        varivelAuxiliar = matrizInversaL[i][j];
                    }
                    matrizInversaL[i][j] = matrizInversaL[i][j] / matriz[i][i];
                }
            }
        }
        return matrizInversaL;
    }

    private static double[][] criarMatrizInversaU(double[][] matriz) {
        double[][] inversaU = new double[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++)
                inversaU[i][j] = (Math.pow(-1, i + j) * calcularDeterminante(calcularFator(matriz, i, j)));
        double det = (double) (1.0 / calcularDeterminante(matriz));
        for (int i = 0; i < inversaU.length; i++) {
            for (int j = 0; j <= i; j++) {
                double temp = inversaU[i][j];
                inversaU[i][j] = inversaU[j][i] * det;
                inversaU[j][i] = temp * det;
            }
        }
        return inversaU;
    }


    private static double calcularDeterminante(double[][] matriz) {
        if (matriz.length != matriz[0].length)
            throw new IllegalStateException("Matriz não quadrada! Impossivel.");
        if (matriz.length == 2)
            return (matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0]);
        double det = 0;
        for (int i = 0; i < matriz[0].length; i++)
            det += Math.pow(-1, i) * matriz[0][i] * calcularDeterminante(calcularFator(matriz, 0, i));
        return det;
    }


    private static double[][] calcularFator(double[][] matriz, int row, int column) {
        double[][] menor = new double[matriz.length - 1][matriz.length - 1];
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; i != row && j < matriz[i].length; j++)
                if (j != column)
                    menor[i < row ? i : i - 1][j < column ? j : j - 1] = matriz[i][j];
        return menor;
    }

    public static void decomposicaoLU(double[][] matriz, double[][] matrizU, double[][] matrizL) {
        int sizeL = matriz.length;
        int sizeC = matriz[0].length;
        for (int j = 0; j < sizeC; ++j) {
            //GERA MATRIZ TRIANGULAR SUPERIOR
            for (int i = 0; i <= j; ++i) {
                if (j == 0) {
                    matrizL[i][j] = matriz[i][j];
                }
                if (i < j) {
                    if (i == 0) {
                        matrizU[i][j] = matriz[i][j] / matriz[i][i];
                    }
                    for (int k = 0; k < i; ++k) {
                        matrizU[i][j] = (matriz[i][j] - matriz[i][k] * matrizU[k][j]) / matrizL[i][i];
                    }
                }
                matrizU[i][i] = 1;
            }
            for (int i = 0; i < sizeL; ++i) {
                //GERA MATRIZ TRIANGULAR INFERIOR

                if (j == 0) {
                    matrizL[i][j] = matriz[i][j];

                } else {
                    double varivelAuxiliar = 0;
                    for (int k = 0; k < j; ++k) {

                        matrizL[i][j] = matriz[i][j] - matrizL[i][k] * matrizU[k][j] + varivelAuxiliar;
                        varivelAuxiliar = matrizL[i][j];
                    }
                    if (j > i) {
                        matrizL[i][j] = 0;
                    }
                }
            }
        }
    }



    public static double[][] criarMatrizQ(double[][] matriz) {
        int sizeL = matriz.length;
        int sizeC = matriz[0].length;

        double[][] matrizQ = new double[sizeL - 1][sizeC - 1]; // a coluna de óbitos não interessa, pelo que vai ter menos uma coluna e linha;

        for (int i = 0; i < sizeC - 1; i++) {
            for (int j = 0; j < sizeL - 1; j++) {
                matrizQ[j][i] = matriz[j][i];
            }
        }
        return matrizQ;
    }


    public static double[][] criarMatrizIdentidade(double[][] matrizQ) {
        int sizeL = matrizQ.length;
        int sizeC = matrizQ[0].length;
        double[][] matrizIdentidade = new double[sizeL][sizeC];

        for (int i = 0; i < sizeC; i++) {
            for (int j = 0; j < sizeL; j++) {
                if (i == j) {
                    matrizIdentidade[j][i] = 1;
                }
            }
        }
        return matrizIdentidade;
    }

    public static double[][] criarMatrizNaoInversa(double[][] matrizQ, double[][] matrizI) {
        int sizeL = matrizQ[0].length;
        int sizeC = matrizQ.length;
        double[][] matrizN = new double[sizeL][sizeC];
        for (int i = 0; i < sizeC; i++) {
            for (int j = 0; j < sizeL; j++) {
                matrizN[j][i] = matrizI[j][i] - matrizQ[j][i];
            }
        }

        return matrizN;
    }

}